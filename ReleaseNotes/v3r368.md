2018-09-26 AppConfig v3r368
========================================

- Add Moore options for MC pHe productions !40 (@rmatev)   
  The new MC TCKs added, as requested by the IFT group (LHCBGAUSS-1479) are:
   * 0x61641724
   * 0x61641725
   * 0x51641724
   * 0x51641725
