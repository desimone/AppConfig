2019-03-07 AppConfig v3r378
========================================
This version is released on 'master' branch.

## New features:
 - Updated option files for Stripping35 (@nskidmor) and for Stripping34r0p1 (@cvazquez). 
