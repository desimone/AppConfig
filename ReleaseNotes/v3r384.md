

2019-07-16 AppConfig v3r384
========================================

This version is released on master branch.

### Other

- stripping tck for second validation, !82 (@nskidmor)   
  

- Change the name of the target branch on CI jobs, !79 (@clemenci)   
  this is most probably needed because of a change in Gitlab which prevents the old version to work
