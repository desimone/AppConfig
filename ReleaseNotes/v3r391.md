

2019-10-23 AppConfig v3r391
========================================

This version is released on master branch.

### Other

- Added option to temporarily fix RootINTES for Gauss sequencers, !102 (@dmuller)   
