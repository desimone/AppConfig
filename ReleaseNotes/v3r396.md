

2020-05-05 AppConfig v3r396
========================================

Release to add options for S35r2 and S35r3
----------------------------------------

This version is released on master branch.


- Add options for S35r2 and S35r3, !116 (@atully)   
  
