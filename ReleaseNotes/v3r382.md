2019-05-27 - AppConfig v3r382
========================================

This version is released on master branch.

### Line developments

- Remove MaxCombinations from S34 and S21rXp2, !77 (@cvazquez)   
  
- New data type2018 PbPb, !76 (@elniel)   
  new files with 2018 DataType for Moore,Boole and Gauss.

- New data type2018, !75 (@elniel)   
  New options files DataType-2018 for Boole, Gauss and Moore.

- LHCb beamfiles PbPb and PbNe 2018, !71 (@sbelin)   
  Add beamfiles for PbPb and PbNe production model
