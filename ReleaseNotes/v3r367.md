2018-09-01 AppConfig v3r367
========================================

- Add Brunel options for running over passthrough data (options/Brunel/Passthrough.py), !38 (@rmatev)   
  Necessary for reco production on 2017 low-mu data. Tested with a new Brunel test: lhcb/Brunel!481
