

2019-12-18 AppConfig v3r393
========================================

Release to update options files for S24r2/S28r2
----------------------------------------

This version is released on master branch.

- options files for S24r2/S28r2 update, !106 (@nskidmor)   
  
