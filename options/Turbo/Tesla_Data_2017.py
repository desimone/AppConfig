"""Configuration for running Tesla over 2017 data.

Requires at least one additional options file defining Tesla().TriggerLines or
Tesla().Streams.
"""
from Configurables import Tesla

Tesla().DataType = '2017'
Tesla().HDRFilter = True
Tesla().InputType = 'RAW'
Tesla().Mode = 'Online'
Tesla().outputSuffix = '.mdst'
Tesla().Simulation = False
