from TurboStreamProd.streams import turbo_streams 
from Configurables import Tesla
Tesla().Streams = turbo_streams["2016"]

#Tesla().ValidateStreams = True
# Turn on Sascha's algorithm ignoring TurboCalib
Tesla().EnableLineChecker = True
Tesla().IgnoredLines = [".*TurboCalib"]
