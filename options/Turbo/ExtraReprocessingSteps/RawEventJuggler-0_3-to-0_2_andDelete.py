from Configurables import GaudiSequencer, RawEventJuggler, ApplicationMgr
jseq=GaudiSequencer("RawEventSplitSeq")
juggler=RawEventJuggler("rdstJuggler")
juggler.Sequencer=jseq
juggler.Input=0.3  # 2015 Online (Moore) format 
juggler.Output=0.2 # Turbo format
juggler.KillExtraNodes=True
juggler.KillExtraBanks=True
juggler.KillExtraDirectories = True
ApplicationMgr().TopAlg = [jseq]
