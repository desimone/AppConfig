from Configurables import Tesla 
from Gaudi.Configuration import *
from TurboStreamProd.helpers import *
from TurboStreamProd import prodDict

### 2016 basis
version='2016_0x21271600'
lines = allLines(prodDict,version,debug=True)

### 2016 June additions
version_add = '2016_0x21331609_additions'
extras = allLines(prodDict,version_add,debug=True)
# take away the lines that have already been added to Turcal
extras.remove('Hlt2PIDDs2PiPhiKKPosTaggedTurboCalib')
extras.remove('Hlt2PIDDs2PiPhiKKNegTaggedTurboCalib')

lines+=extras

Tesla().TriggerLines = lines
