"""Configure Tesla to propagate decisions only for specific HLT2 lines."""
from Configurables import Tesla

Tesla().KillInputHlt2Reps = True
Tesla().InputHlt2RepsToKeep = [
    'Hlt2CharmHadInclDst2PiD02HHXBDTDecision',
    'Hlt2CharmHadInclLcpToKmPpPipBDTDecision',
    'Hlt2CharmHadInclSigc2PiLc2HHXBDTDecision',
    'Hlt2Topo2BodyDecision',
    'Hlt2Topo3BodyDecision',
    'Hlt2Topo4BodyDecision',
    'Hlt2TopoE2BodyDecision',
    'Hlt2TopoE3BodyDecision',
    'Hlt2TopoE4BodyDecision',
    'Hlt2TopoEE2BodyDecision',
    'Hlt2TopoEE3BodyDecision',
    'Hlt2TopoEE4BodyDecision',
    'Hlt2TopoMu2BodyDecision',
    'Hlt2TopoMu3BodyDecision',
    'Hlt2TopoMu4BodyDecision',
    'Hlt2TopoMuE2BodyDecision',
    'Hlt2TopoMuE3BodyDecision',
    'Hlt2TopoMuE4BodyDecision',
    'Hlt2TopoMuMu2BodyDecision',
    'Hlt2TopoMuMu3BodyDecision',
    'Hlt2TopoMuMu4BodyDecision',
    'Hlt2TopoMuMuDDDecision'
]
