from Configurables import Tesla 
from TeslaTools.TeslaMonitor import TeslaH1, TeslaH2, TeslaH3


class StdCut:
  TrackGhostProb = "(MAXTREE ( TRGHP , ISBASIC ) < 0.4)"
  
  DstCut = ( TrackGhostProb + 
        "& CHILDCUT ( (abs(WM('pi+','pi-') - PDGMASS) > 25) " + 
                   "& (abs(WM('K+','K-')   - PDGMASS) > 25)   " + 
                   "& (abs(WM('pi+','K-')  - PDGMASS) > 25) , 1 )"  )
  
  PosId = "& (ID > 0)"
  NegId = "& (ID < 0)"
  
  def _WMr ( oldId, newId, minMass, maxMass ):
    "Wrong mass for the phi daughters, when coming from a Ds+"
    mass={'p+': 938.272046,'p~-':938.272046, 
          'K+': 493.667,    'K-':493.667, 
          'pi+':139.57018, 'pi-':139.57018}; 
  
    var = "sqrt( pow( ( E - sqrt(pow({mass},2) + pow(MINTREE(ID == '{oldId}',P),2)) + MINTREE(ID == '{oldId}', E) ),2) - pow(P,2) )"
    return " ( ( {var} < {min} ) | ( {var} > {max} ) )".format (
        min = minMass, max = maxMass,
        var = var.format (newId = newId, oldId = oldId, 
                          mass = mass[newId])
      )
  
  
  DsPhiVetos =  (TrackGhostProb + 
        " & (  ( (ID > 0) &  {} & {} ) | ( (ID < 0) &  {} & {} ) )" 
          .format ( _WMr ("K+", "pi+", 1830, 1890), _WMr ("K+", "p+" , 2266, 2306),
                    _WMr ("K-", "pi-", 1830, 1890), _WMr ("K-", "p~-", 2266, 2306))
     )
  
  
  
  Jpsiee = "&" + " & ".join([
      "(BPVIPCHI2()<9.0) ",
      "(VFASPF(VCHI2/VDOF)<9) ",
      "(abs(MM-CHILD(MM, 1)-2182.3)<100) ",
      "(inRange(2250, CHILD(MM, 1), 3600)) ",
      "(NINTREE(('e-'==ABSID) & (BPVIPCHI2()>25)) == 2)"
    ])
  
  


cut         = StdCut.TrackGhostProb
DstCut      = StdCut.DstCut
PosId       = StdCut.PosId
NegId       = StdCut.NegId
DsPhiVetos  = StdCut.DsPhiVetos
Jpsiee      = StdCut.Jpsiee




## Format:  TurboLine: Particle or decaydescriptor: LoKi variable[: other vars]
Tesla().Monitors =  [

    # DSt vs Dz Pos
    TeslaH2 ( "Hlt2PIDD02KPiTagTurboCalib:D*(2010)+:CHILD(M,1):M-CHILD(M,1)",
              ";m(K^{-}#pi^{+}) [MeV/c^{2}]; m(D^{0}#pi^{+}) - m(K^{-}#pi^{+}) [MeV/c^{2}]",
              300, 1825, 1910, 300, 141, 153, DstCut + PosId,
              histname = "DstDz_Pos").getAlgorithm()
    # DSt vs Dz Neg
    , TeslaH2 ( "Hlt2PIDD02KPiTagTurboCalib:D*(2010)+:CHILD(M,1):M-CHILD(M,1)",
              ";m(K^{-}#pi^{+}) [MeV/c^{2}]; m(D^{0}#pi^{+}) - m(K^{-}#pi^{+}) [MeV/c^{2}]",
              300, 1825, 1910, 300, 141, 153, DstCut + NegId, 
              histname = "DstDz_Neg").getAlgorithm()

    # DSt vs Dz (4 body) Pos  -- Contact: Federico.Redi@cern.ch
    , TeslaH2 ( "Hlt2PIDD02KPiPiPiTagTurboCalib:D*(2010)+:CHILD(M,1):M-CHILD(M,1)",
              ";m(K^{-}#pi^{+}#pi^{-}#pi^{+}) [MeV/c^{2}]"+
              "; m(D^{0}#pi^{+}) - m(K^{-}#pi^{+}) [MeV/c^{2}]",
              300, 1800, 1920, 300, 139, 160, cut + PosId,
              histname = "DstDz_K3Pi_Pos").getAlgorithm()
    # DSt vs Dz (4 body) Neg  -- Contact: Federico.Redi@cern.ch
    , TeslaH2 ( "Hlt2PIDD02KPiPiPiTagTurboCalib:D*(2010)+:CHILD(M,1):M-CHILD(M,1)",
              ";m(K^{-}#pi^{+}#pi^{-}#pi^{+}) [MeV/c^{2}]"+
              "; m(D^{0}#pi^{+}) - m(K^{-}#pi^{+}) [MeV/c^{2}]",
              300, 1800, 1920, 300, 139, 160, cut + NegId, 
              histname = "DstDz_K3Pi_Neg").getAlgorithm()
    
    # Jpsi
       # Negative Tag (positive probe)
    , TeslaH1 ( "Hlt2PIDDetJPsiMuMuNegTaggedTurboCalib:J/psi(1S):M" ,
                ";m(#mu#mu) [MeV/c^{2}]; Candidates",
              1000, 3096 - 110, 3096 + 110,cut, histname = "Jpsi_Pos").getAlgorithm()
       # Positive Tag (negative probe)
    , TeslaH1 ( "Hlt2PIDDetJPsiMuMuPosTaggedTurboCalib:J/psi(1S):M" ,
                ";m(#mu#mu) [MeV/c^{2}]; Candidates",
              1000, 3096 - 110, 3096 + 110,cut, histname = "Jpsi_Neg").getAlgorithm()

    # Lambda0
       # Low PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+PosId, histname = "L0_Pos").getAlgorithm()
       # High PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLhighPTTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+PosId, histname = "L0HPT_Pos").getAlgorithm()
       # Highest PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLveryhighPTTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+PosId, histname = "L0VHPT_Pos").getAlgorithm()
      # isMuon (unprescaled)
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLisMuonTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+PosId, histname = "L0muon_Pos").getAlgorithm()

    # Lambda~0
       # Low PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+NegId, histname = "L0_Neg").getAlgorithm()
       # High PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLhighPTTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+NegId, histname = "L0HPT_Neg").getAlgorithm()
       # Highest PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLveryhighPTTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+NegId, histname = "L0VHPT_Neg").getAlgorithm()
      # isMuon (unprescaled)
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLisMuonTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+NegId, histname = "L0muon_Neg").getAlgorithm()


    # Phi(1020) -- Contact: Ivan.Polyakov@cern.ch
      # positive tag
    , TeslaH1 ( "Hlt2PIDDetPhiKKPosTaggedTurboCalib:phi(1020):M",
                ";m(K^{+}K^{-}) [MeV/c^{2}]; Candidates",
               40*5, 1000, 1040, cut, histname = "Phi_KM").getAlgorithm() 

      # negative tag
    , TeslaH1 ( "Hlt2PIDDetPhiKKNegTaggedTurboCalib:phi(1020):M",
                ";m(K^{+}K^{-}) [MeV/c^{2}]; Candidates",
               40*5, 1000, 1040, cut, histname = "Phi_KP").getAlgorithm() 

    # D_s+ -> phi(1020) pi+ -- Contact: Ivan.Polyakov@cern.ch               
      #unbiased                                                            
    , TeslaH2 ( "Hlt2PIDDs2PiPhiKKUnbiasedTurboCalib:D_s+:CHILD(M,1):M",  
                ";m(K^{+}K^{-}) [MeV/c^{2}];m(#phi#pi^{+}) [MeV/c^{2}]",  
                39*10, 1000, 1039, 140*5, 1898, 2038, DsPhiVetos, 
                histname = "DsPhi_K_NoTag").getAlgorithm()
      #unbiased  DEBUG                                                          
    , TeslaH2 ( "Hlt2PIDDs2PiPhiKKUnbiasedTurboCalib:D_s+:CHILD(M,1):M",  
                ";m(K^{+}K^{-}) [MeV/c^{2}];m(#phi#pi^{+}) [MeV/c^{2}]",  
                39*10, 1000, 1039, 140*5, 1898, 2038, cut,
                histname = "DsPhi_K_NoTag_NOVETOES").getAlgorithm()

################################################################################
## Ds -> K K pi

      # positive tag                                                    
    , TeslaH2 ( "Hlt2PIDDs2PiPhiKKPosTaggedTurboCalib:D_s+:CHILD(M,1):M",  
                ";m(K^{+}K^{-}) [MeV/c^{2}];m(#phi#pi^{+}) [MeV/c^{2}]",  
                40*10, 1000, 1800, 140*5, 1900, 2040, cut + PosId, 
                histname = "DsPhi_Neg").getAlgorithm()
                                                                                        
      # negative tag                                                      
    , TeslaH2 ( "Hlt2PIDDs2PiPhiKKNegTaggedTurboCalib:D_s+:CHILD(M,1):M",  
                ";m(K^{+}K^{-}) [MeV/c^{2}];m(#phi#pi^{+}) [MeV/c^{2}]",  
                40*10, 1000, 1800, 140*5, 1900, 2040, cut + NegId, 
                histname = "DsPhi_Pos").getAlgorithm()

      # positive tag                                                    
    , TeslaH2 ( "Hlt2PIDDs2KKPiSSTaggedTurboCalib:D_s+:M12:M",  
                ";m(K^{+}K^{-}) [MeV/c^{2}];m(K^{+}K^{-}#pi^{+}) [MeV/c^{2}]",  
                40*10, 1000, 1800, 140*5, 1900, 2040, cut + PosId, 
                histname = "DsKKPi_Neg").getAlgorithm()
                                                                                        
      # negative tag                                                      
    , TeslaH2 ( "Hlt2PIDDs2KKPiSSTaggedTurboCalib:D_s+:M12:M",  
                ";m(K^{+}K^{-}) [MeV/c^{2}];m(K^{+}K^{-}#pi^{+}) [MeV/c^{2}]",  
                40*10, 1000, 1800, 140*5, 1900, 2040, cut + NegId, 
                histname = "DsKKPi_Pos").getAlgorithm()

      # positive tag                                                    
    , TeslaH1 ( "Hlt2PIDDs2KKPiSSTaggedTurboCalib:D_s+:M12",  
                ";m(K^{+}K^{-}) [MeV/c^{2}];m(K^{+}K^{-}#pi^{+}) [MeV/c^{2}]",  
                40*10, 1000, 1800, cut + PosId, 
                histname = "DsKKPi_Phi_Neg").getAlgorithm()
                                                                                        
      # negative tag                                                      
    , TeslaH1 ( "Hlt2PIDDs2KKPiSSTaggedTurboCalib:D_s+:M12",  
                ";m(K^{+}K^{-}) [MeV/c^{2}];m(K^{+}K^{-}#pi^{+}) [MeV/c^{2}]",  
                40*10, 1000, 1800, cut + NegId, 
                histname = "DsKKPi_Phi_Pos").getAlgorithm()

      # positive tag                                                    
    , TeslaH1 ( "Hlt2PIDDs2KKPiSSTaggedTurboCalib:D_s+:M",  
                ";m(K^{+}K^{-}) [MeV/c^{2}];m(K^{+}K^{-}#pi^{+}) [MeV/c^{2}]",  
                140*5, 1900, 2040, cut + PosId, 
                histname = "DsKKPi_Ds_Neg").getAlgorithm()
                                                                                        
      # negative tag                                                      
    , TeslaH1 ( "Hlt2PIDDs2KKPiSSTaggedTurboCalib:D_s+:M",  
                ";m(K^{+}K^{-}) [MeV/c^{2}];m(K^{+}K^{-}#pi^{+}) [MeV/c^{2}]",  
                140*5, 1900, 2040, cut + NegId, 
                histname = "DsKKPi_Ds_Pos").getAlgorithm()

################################################################################
## Ds -> mu mu pi

      # positive tag                                                    
    , TeslaH2 ( "Hlt2PIDDs2PiPhiMuMuPosTaggedTurboCalib:D_s+:CHILD(M,1):M",  
                ";m(#mu^{+}#mu^{-}) [MeV/c^{2}];m(#phi#pi^{+}) [MeV/c^{2}]",  
                40*10, 1000, 1800, 140*5, 1900, 2040, cut + PosId, 
                histname = "DsPhiMuMu_Neg").getAlgorithm()
                                                                                        
      # negative tag                                                      
    , TeslaH2 ( "Hlt2PIDDs2PiPhiMuMuNegTaggedTurboCalib:D_s+:CHILD(M,1):M",  
                ";m(#mu^{+}#mu^{-}) [MeV/c^{2}];m(#phi#pi^{+}) [MeV/c^{2}]",  
                40*10, 1000, 1800, 140*5, 1900, 2040, cut + NegId, 
                histname = "DsPhiMuMu_Pos").getAlgorithm()

      # positive tag                                                    
    , TeslaH2 ( "Hlt2PIDDs2MuMuPiPosTaggedTurboCalib:D_s+:M12:M",  
                ";m(#mu^{+}#mu^{-}) [MeV/c^{2}];m(#mu^{+}#mu^{-}#pi^{+}) [MeV/c^{2}]",  
                40*10, 1000, 1800, 140*5, 1900, 2040, cut + PosId, 
                histname = "DsMuMuPi_Neg").getAlgorithm()
                                                                                        
      # negative tag                                                      
    , TeslaH2 ( "Hlt2PIDDs2MuMuPiNegTaggedTurboCalib:D_s+:M12:M",  
                ";m(#mu^{+}#mu^{-}) [MeV/c^{2}];m(#mu^{+}#mu^{-}#pi^{+}) [MeV/c^{2}]",  
                40*10, 1000, 1800, 140*5, 1900, 2040, cut + NegId, 
                histname = "DsMuMuPi_Pos").getAlgorithm()

      # positive tag                                                    
    , TeslaH1 ( "Hlt2PIDDs2MuMuPiPosTaggedTurboCalib:D_s+:M12",  
                ";m(#mu^{+}#mu^{-}) [MeV/c^{2}];m(#mu^{+}#mu^{-}#pi^{+}) [MeV/c^{2}]",  
                40*10, 1000, 1800, cut + PosId, 
                histname = "DsMuMuPi_Phi_Neg").getAlgorithm()
                                                                                        
      # negative tag                                                      
    , TeslaH1 ( "Hlt2PIDDs2MuMuPiNegTaggedTurboCalib:D_s+:M12",  
                ";m(#mu^{+}#mu^{-}) [MeV/c^{2}];m(#mu^{+}#mu^{-}#pi^{+}) [MeV/c^{2}]",  
                40*10, 1000, 1800, cut + NegId, 
                histname = "DsMuMuPi_Phi_Pos").getAlgorithm()

      # positive tag                                                    
    , TeslaH1 ( "Hlt2PIDDs2MuMuPiPosTaggedTurboCalib:D_s+:M",  
                ";m(#mu^{+}#mu^{-}) [MeV/c^{2}];m(#mu^{+}#mu^{-}#pi^{+}) [MeV/c^{2}]",  
                140*5, 1900, 2040, cut + PosId, 
                histname = "DsMuMuPi_Ds_Neg").getAlgorithm()
                                                                                        
      # negative tag                                                      
    , TeslaH1 ( "Hlt2PIDDs2MuMuPiNegTaggedTurboCalib:D_s+:M",  
                ";m(#mu^{+}#mu^{-}) [MeV/c^{2}];m(#mu^{+}#mu^{-}#pi^{+}) [MeV/c^{2}]",  
                140*5, 1900, 2040, cut + NegId, 
                histname = "DsMuMuPi_Ds_Pos").getAlgorithm()


  ################################################################################
    # B -> J/psi (ee) K -- Contact: Dianne and Federico Redi
      # negative tag
    , TeslaH2 ( "Hlt2PIDB2KJPsiEENegTaggedTurboCalib:B+:M:CHILD(M,1)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(#mu#mu) [MeV/c^{2}]",
                20*10, 4300, 5800, 20*10, 2300, 3400,
                cut + Jpsiee, histname = "BJpsi_Pos").getAlgorithm()

      # positive tag
    , TeslaH2 ( "Hlt2PIDB2KJPsiEEPosTaggedTurboCalib:B+:M:CHILD(M,1)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(#mu#mu) [MeV/c^{2}]",
                20*10, 4300, 5800, 20*10, 2300, 3400,
                cut + Jpsiee, histname = "BJpsi_Neg").getAlgorithm()

    # B -> J/psi (ee) K USING DELTA MASS -- Contact: Dianne and Federico Redi
      # negative tag
    , TeslaH2 ( "Hlt2PIDB2KJPsiEENegTaggedTurboCalib:B+:M-CHILD(M,1):CHILD(M,1)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(#mu#mu) [MeV/c^{2}]",
                20*10, 2182.3-100., 2182.3+100., 20*10, 2300, 3400,
                cut + Jpsiee, histname = "BJpsi_deltaPos").getAlgorithm()

      # positive tag
    , TeslaH2 ( "Hlt2PIDB2KJPsiEEPosTaggedTurboCalib:B+:M-CHILD(M,1):CHILD(M,1)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(#mu#mu) [MeV/c^{2}]",
                20*10, 2182.3-100., 2182.3+100., 20*10, 2300, 3400,
                cut + Jpsiee, histname = "BJpsi_deltaNeg").getAlgorithm()
    
    # KS0 -- Contact: j.devries@cern.ch                                   
      # positive tag                                                      
    , TeslaH1 ( "Hlt2PIDKs2PiPiLLTurboCalib:KS0:M",                       
                ";m(#pi^{+}#pi^{-}) [MeV/c^{2}]; Candidates",                               
                1000, 470, 525, cut, histname = "KS0").getAlgorithm() 

    # B -> J/psi (mumu) K -- Contact: Dianne
    , TeslaH2 ( "Hlt2PIDB2KJPsiMuMuPosTaggedTurboCalib:B+:M:CHILD(M,1)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(#mu#mu) [MeV/c^{2}]",
                90*5, 5000, 5400, 90*5, 3000, 3200,
                cut, histname = "BJpsi_Neg").getAlgorithm()

    , TeslaH2 ( "Hlt2PIDB2KJPsiMuMuNegTaggedTurboCalib:B+:M:CHILD(M,1)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(#mu#mu) [MeV/c^{2}]",
                90*5, 5000, 5400, 90*5, 3000, 3200,
                cut, histname = "BJpsi_Pos").getAlgorithm()
    

#    # Lambda_b -> Lambda_c mu nu   with   Lambda_c -> ^p K pi
    , TeslaH1 ( "Hlt2PIDLb2LcMuNuTurboCalib:Lambda_c+:M",
                ";m(pK^{-}#pi^{+}) [MeV/c^{2}]; Candidates",
                870, 2215, 2360, cut+PosId, histname = "LbLcMu_Pos" ).getAlgorithm()
                
#    # Lambda_b -> Lambda_c pi   with   Lambda_c -> ^p K pi
    , TeslaH1 ( "Hlt2PIDLb2LcPiTurboCalib:Lambda_c+:M",
                ";m(pK^{-}#pi^{+}) [MeV/c^{2}]; Candidates",
                870, 2215, 2360, cut+PosId, histname = "LbLcPi_Pos" ).getAlgorithm()

#    # Lambda_c -> ^p K pi
    , TeslaH1 ( "Hlt2PIDLc2KPPiTurboCalib:Lambda_c+:M",
                ";m(pK^{-}#pi^{+}) [MeV/c^{2}]; Candidates",
                870, 2215, 2360, cut+PosId, histname = "Lc_Pos" ).getAlgorithm()
#    # Lambda_b -> Lambda_c mu nu   with   Lambda_c -> ^p K pi
    , TeslaH1 ( "Hlt2PIDLb2LcMuNuTurboCalib:Lambda_c+:M",
                ";m(pK^{-}#pi^{+}) [MeV/c^{2}]; Candidates",
                870, 2215, 2360, cut+NegId, histname = "LbLcMu_Neg" ).getAlgorithm()
                
#    # Lambda_b -> Lambda_c pi   with   Lambda_c -> ^p K pi
    , TeslaH1 ( "Hlt2PIDLb2LcPiTurboCalib:Lambda_c+:M",
                ";m(pK^{-}#pi^{+}) [MeV/c^{2}]; Candidates",
                870, 2215, 2360, cut+NegId, histname = "LbLcPi_Neg" ).getAlgorithm()

#    # Lambda_c -> ^p K pi
    , TeslaH1 ( "Hlt2PIDLc2KPPiTurboCalib:Lambda_c+:M",
                ";m(pK^{-}#pi^{+}) [MeV/c^{2}]; Candidates",
                870, 2215, 2360, cut+NegId, histname = "Lc_Neg" ).getAlgorithm()





    ## Monitoring only
    , TeslaH1 ( "Hlt2PIDD02KPiTagTurboCalib:D*(2010)+:CHILD(M,1)",
              ";m(D^{0}#pi^{+}) [MeV/c^{2}]; Candidates",
              1000, 1865 - 60, 1865 + 60, cut).getAlgorithm()
    , TeslaH1 ( "Hlt2PIDD02KPiTagTurboCalib:D*(2010)+:M",
              ";m(K^{-}#pi^{+}) [MeV/c^{2}]; Candidates",
              1000, 1865 + 139 - 60, 1865 + 155 + 60, cut ).getAlgorithm()
    , TeslaH1 ( "Hlt2PIDD02KPiTagTurboCalib:D*(2010)+:M-CHILD(M,1)",
              ";m(D^{0}#pi^{+}) - m(K^{-}#pi^{+}) [MeV/c^{2}]; Candidates",
              1000, 139, 155, cut ).getAlgorithm()
    ]

