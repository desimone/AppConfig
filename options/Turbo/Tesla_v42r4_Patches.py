import Tesla.Configuration
from Configurables import HltSelReportsWriter, HltSelReportsStripper
Tesla.Configuration.HltSelReportsWriter = HltSelReportsWriter

from DAQSys.Decoders import DecoderDB
selreports_decoder = DecoderDB['HltSelReportsDecoder/Hlt2SelReportsDecoder']
selreports_loc = selreports_decoder.Outputs['OutputHltSelReportsLocation']
stripper = HltSelReportsStripper('Hlt2SelReportsStripper')
stripper.InputHltSelReportsLocation = selreports_loc

