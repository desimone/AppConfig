from Configurables import Moore
Moore().UseTCK = True # provide an invalid TCK here so one is forced to append eg. Conditions/TCK-0x00051810.py
Moore().InitialTCK = '0x00000000'
Moore().L0 = True
Moore().RunL0Emulator = True
Moore().UseDBSnapshot = False
Moore().EnableRunChangeHandler = False

Moore().CheckOdin = False
Moore().WriterRequires = []

Moore().Simulation = True

from Configurables import L0MuonAlg
L0MuonAlg( "L0Muon" ).L0DUConfigProviderType = "L0DuConfigProvider"
