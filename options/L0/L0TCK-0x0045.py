# L0 TCK for 2012 simulations
from Configurables import L0Conf
L0Conf().TCK = '0x0045'
from L0DU.L0Algs import emulateL0Muon
l0muon = emulateL0Muon()
l0muon.LUTVersion = "V3"
