# L0 TCK for 2011 simulations
from Configurables import L0Conf
L0Conf().TCK = '0x0040'
from L0DU.L0Algs import emulateL0Muon
l0muon = emulateL0Muon()
l0muon.LUTVersion = "V2"
