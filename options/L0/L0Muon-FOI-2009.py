# Options to adjust the FOI for emulation of 2009 and early 2010 L0Muon trigger
from Configurables import L0MuonAlg
l0mu = L0MuonAlg("L0Muon")
l0mu.FoiXSize = [6,5,0,12,12]
l0mu.FoiYSize = [0,0,0,1,1]
