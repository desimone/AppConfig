#
# A change in the IODataManager().AgeLimit in the DaVinci configurable
# has made the FSR Conv Service segfault at the end of the job
#
# This is exposed whenever an output file is generated from more than one
# input file, with FSRs required on the output file.
#
# See bug #75795 for details
#
from Gaudi.Configuration import *
IODataManager().AgeLimit=2
#
# 2 is the default from the C++ class.
#
