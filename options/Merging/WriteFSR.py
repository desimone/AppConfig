#
# Required for writing out FSR. The definition of the output 
# file happens in Dirac since this changes for each production job.
# This solves https://savannah.cern.ch/task/index.php?15176
#
from Gaudi.Configuration import *
from Configurables import RecordStream

FileRecords = RecordStream("FileRecords")
FileRecords.ItemList         = ["/FileRecords#999"]
FileRecords.EvtDataSvc       = "FileRecordDataSvc"
FileRecords.EvtConversionSvc = "FileRecordPersistencySvc"

ApplicationMgr().OutStream.append(FileRecords)
