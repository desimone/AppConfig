##
## Job options to copy one or more event data input files onto an output file
##

from Gaudi.Configuration import *
from Configurables import LHCbApp, LbAppInit

LHCbApp()

# This give a nice summary at the end
#MyAlg = LbAppInit()
#ApplicationMgr().TopAlg += [ MyAlg ]

ApplicationMgr().TopAlg += ['InputCopyStream']

EventSelector().PrintFreq = 10000

## Provide one or more input files.
# EventSelector().Input = [
#  "DATAFILE='PFN:MyInputFile.dst' TYP='POOL_ROOTTREE' OPT='READ'"
# ]

## Define the output file
# OutputStream("InputCopyStream").Output =
#    "DATAFILE='PFN:MyOutputFile.dst' TYP='POOL_ROOTTREE' OPT='RECREATE'"

