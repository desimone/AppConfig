from Gaudi.Configuration import *
from Configurables import DaVinci, LumiAlgsConf

DaVinci().InputType = 'MDST'
DaVinci().DataType = '2012'
DaVinci().UserAlgorithms = [ InputCopyStream() ]

######################### CAUTION ######################
# Seriously, this next line is for production jobs only
# Don't cut and paste it for anything else please!
LumiAlgsConf().SetFSRStatus = "VERIFIED"
# Thanks!
########################################################

#Merge all low-level FSRs into a single top-level FSR
#This can save 75% of the file size of small files,
#and it prevents the memory explosion at the end of user jobs
if 'MergeFSR' in DaVinci.__slots__:
    DaVinci().MergeFSR = True
elif 'MergeFSR' in LumiAlgsConf.__slots__:
    LumiAlgsConf().MergeFSR = True

#
# Required for writing out FSR. The definition of the output 
# file happens in Dirac since this changes for each production job.
# This solves https://savannah.cern.ch/task/index.php?15176
#
from Configurables import RecordStream

FileRecords = RecordStream("FileRecords")
FileRecords.ItemList         = ["/FileRecords#999"]
FileRecords.EvtDataSvc       = "FileRecordDataSvc"
FileRecords.EvtConversionSvc = "FileRecordPersistencySvc"

ApplicationMgr().OutStream.append(FileRecords)

