from Gaudi.Configuration import *
import re
def FixFSROutput():
    """
    Add the correct option ("RECREATE") the the Output property
    This is a massive HACK!
    """
    fileopt = re.compile("OPT='([^']*)'")
    try:
        FileRecords = allConfigurables["FileRecords"]
        opt = re.search("OPT='([^']*)'", FileRecords.Output)
        if not opt:
            FileRecords.Output += " OPT='RECREATE'"
        elif not "RECREATE".startswith(opt.group(1).upper()):
            a,b = opt.span(1)
            s = FileRecords.Output
            FileRecords.Output = s[:a] + "RECREATE" + s[b:]
    except:
        log.warning("Cannot fix FileRecords Output")

appendPostConfigAction(FixFSROutput)
