#
# Raw event juggler to be able to rerun reconstruction in S24r2 and S28r2
# and splitted RawEvent 
#
from Configurables import GaudiSequencer, RawEventJuggler
jseq=GaudiSequencer("RawEventSplitSeq")
juggler=RawEventJuggler("rdstJuggler")
juggler.Sequencer=jseq
juggler.Input=4.3 
juggler.Output=4.3

from Configurables import DaVinci
DaVinci().prependToMainSequence( [jseq] )
