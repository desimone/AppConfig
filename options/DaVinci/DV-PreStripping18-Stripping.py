"""
Options for building PreStripping18.
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='prestripping18'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)

from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()



# Full DST streams to be duplicated into mDST. 
#fullDSTStreams = ['BhadronCompleteEvent', 'Dimuon', 'EW', 'Radiative', 'Semileptonic']
fullDSTStreams = ['BhadronCompleteEvent', 'Dimuon', 'Radiative', 'Semileptonic']

# Append all lines from full DST streams to the test stream
MDSTTestStream = StrippingStream("MDSTTest", 
                                 MaxCandidates = 2000, 
                                 AcceptBadEvents = False, 
                                 BadEventSelection = filterBadEvents, 
                                 TESPrefix = 'Strip')

for stream in streams : 
    if stream.name() in fullDSTStreams : 
        MDSTTestStream.appendLines( stream.lines )
    if stream.name() == 'CharmToBeSwum' : charmToBeSwumStream = stream


# Charm control stream, for 10% of events from CharmToBeSwum
CharmControlStream = StrippingStream("CharmControl", 
                                 MaxCandidates = 2000, 
                                 AcceptBadEvents = False, 
                                 BadEventSelection = filterBadEvents, 
                                 TESPrefix = 'Strip')

CharmControlStream.appendLines( cloneLinesFromStream( charmToBeSwumStream, 'CharmControl', prescale = 0.1 ) )


sc = StrippingConf( Streams = streams,
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents, 
                    TESPrefix = 'Strip' )

# Cannot add these streams to StrippingConf since the unique locations check will fail. 
# Leave them standalone, but first need to create all sequencers and filters.
MDSTTestStream.createConfigurables()
CharmControlStream.createConfigurables()


enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements,
                                      stripMicroDSTStreamConf,
                                      stripMicroDSTElements,
                                      stripCalibMicroDSTStreamConf
                                      )

#
# Configuration of MicroDST
#
mdstStreamConf = stripMicroDSTStreamConf(pack=enablePacking)
mdstElements   = stripMicroDSTElements(pack=enablePacking)

leptonicMicroDSTname   = 'Leptonic'
charmMicroDSTname      = 'Charm'
pidMicroDSTname        = 'PID' 
bhadronMicroDSTname    = 'Bhadron' 
testMicroDSTname       = 'MDSTTest' 

#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking),
    charmMicroDSTname       : mdstElements,
    leptonicMicroDSTname    : mdstElements,
    pidMicroDSTname         : mdstElements,
    bhadronMicroDSTname     : mdstElements,
    testMicroDSTname        : mdstElements, 
    }


SelDSTWriterConf = {
    'default'                : stripDSTStreamConf(pack=enablePacking),
    charmMicroDSTname        : mdstStreamConf,
    leptonicMicroDSTname     : mdstStreamConf,
    bhadronMicroDSTname      : mdstStreamConf,
    testMicroDSTname         : mdstStreamConf,
    pidMicroDSTname          : stripCalibMicroDSTStreamConf(pack=enablePacking)
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams() + [ MDSTTestStream, CharmControlStream ]
#                          SelectionSequences = sc.activeStreams() + [ CharmControlStream ]
                          )


# Add cone variables to Bhadron and EW streams
from Configurables import AddExtraInfo, ConeVariables, ConeVariablesForEW

bhadronOutputLocations = []
ewOutputLocations = []

for stream in streams :
    if stream.name() == 'Bhadron' : bhadronOutputLocations = [ "/Event/" + x for x in stream.outputLocations() ]
    if stream.name() == 'EW' : ewOutputLocations = [ "/Event/" + x for x in stream.outputLocations() ]

bhadron_extra = AddExtraInfo('BhadronExtraInfo')
bhadron_extra.Inputs = bhadronOutputLocations
bhadron_extra.OutputLevel = ERROR

cv1   = ConeVariables('ConeVariables1', ConeAngle = 0.5, ConeNumber = 1)
cv2   = ConeVariables('ConeVariables2', ConeAngle = 1.0, ConeNumber = 2)
bhadron_extra.addTool( cv1 , 'ConeVariables/ConeVariables1')  
bhadron_extra.addTool( cv2 , 'ConeVariables/ConeVariables2')  
bhadron_extra.Tools = [ 'ConeVariables/ConeVariables1', 'ConeVariables/ConeVariables2' ]

ew_extra = AddExtraInfo('EWExtraInfo')
ew_extra.Inputs = ewOutputLocations
ew_extra.OutputLevel = ERROR

cvew1   = ConeVariablesForEW('ConeVariablesForEW1', ConeAngle = 0.3, ConeNumber = 1)
cvew2   = ConeVariablesForEW('ConeVariablesForEW2', ConeAngle = 0.4, ConeNumber = 2)
cvew3   = ConeVariablesForEW('ConeVariablesForEW3', ConeAngle = 0.5, ConeNumber = 3)
ew_extra.addTool( cvew1 , 'ConeVariablesForEW/ConeVariablesForEW1')  
ew_extra.addTool( cvew2 , 'ConeVariablesForEW/ConeVariablesForEW2')  
ew_extra.addTool( cvew3 , 'ConeVariablesForEW/ConeVariablesForEW3')  
ew_extra.Tools = [ 'ConeVariables/ConeVariablesForEW1', 'ConeVariables/ConeVariablesForEW2', 'ConeVariables/ConeVariablesForEW3' ]


#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'SDST'
DaVinci().DataType = "2011"
DaVinci().EvtMax = 10000                        # Number of events
#DaVinci().PrintFreq = 10
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ bhadron_extra, ew_extra ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

#=============================================================
# Temporary for stripping pre-18 test
#=============================================================
# Remove track states
from Configurables import TrackToDST
# Filter Best Track States to be written
trackFilter = TrackToDST("FilterBestTrackStates")
# Filter Muon Track States
muonTrackFilter = TrackToDST("FilterMuonTrackStates")
muonTrackFilter.TracksInContainer = "/Event/Rec/Track/Muon"
# Add to DV Pre event filter sequence
DaVinci().EventPreFilters += [ trackFilter, muonTrackFilter ]
#=============================================================

#importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco12_Run97120_SDST.py")

