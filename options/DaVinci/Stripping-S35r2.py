'''Global configuration for Stripping35r2.'''

from Configurables import Stripping, TrackSys

TrackSys().GlobalCuts = { 'Velo':20000, 'IT':999999, 'OT':999999 }

Stripping().Version = 'Stripping35r2'
Stripping().TCK = 0x44107352
Stripping().MaxCandidates = 2000
