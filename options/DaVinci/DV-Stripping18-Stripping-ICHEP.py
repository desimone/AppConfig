"""
Options for building Stripping18, streams for ICHEP
with default track chi2 cut (<5). 
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping18'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = []
_ichep = buildStream(stripping = config, streamName= 'ICHEP', archive = archive)
_pid   = buildStream(stripping = config, streamName= 'PID'  , archive = archive)
_calib = buildStream(stripping = config, streamName= 'Calibration'  , archive = archive)

streams.append( _ichep)
streams.append( _pid)
streams.append( _calib)

from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = streams,
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents, 
                    TESPrefix = 'Strip' )

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements,
                                      stripMicroDSTStreamConf,
                                      stripMicroDSTElements,
                                      stripCalibMicroDSTStreamConf
                                      )

#
# Configuration of MicroDST
#
mdstStreamConf = stripMicroDSTStreamConf(pack=enablePacking)
mdstElements   = stripMicroDSTElements(pack=enablePacking)

pidMicroDSTname        = 'PID' 

#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking),
    pidMicroDSTname         : mdstElements
    }


SelDSTWriterConf = {
    'default'                : stripDSTStreamConf(pack=enablePacking),
    pidMicroDSTname          : stripCalibMicroDSTStreamConf(pack=enablePacking)
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )


#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'SDST'
DaVinci().DataType = "2012"
DaVinci().EvtMax = -1                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
