from Gaudi.Configuration import *

from CommonParticles.Utils import DefaultTrackingCuts

DefaultTrackingCuts().useCutSet("NULL")
# DefaultTrackingCuts().Types = ["Long","Downstream","Upstream"]

# The first line suppresses all track quality cuts. The second changes the 
# default track types that are selected.
# You can also manually tweak the cuts directly with lines like

DefaultTrackingCuts().Cuts = { "Chi2Cut"       : [  0,    100    ]   # changed
                               ,"LikelihoodCut" : [ -100,  9e40  ]
                               ,"CloneDistCut"  : [ -1e10, 9e40  ]
                               ,"GhostProbCut"  : [ -1,    0.99  ]
                               }

from Configurables import SelDSTWriter, DaVinci
from StrippingConf.Configuration import StrippingConf

from StrippingSelections.Streams import allStreams
from StrippingSelections import StreamMiniBias

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Stripping job configuration
#

noMB = allStreams
noMB.remove( StreamMiniBias.stream )

sc = StrippingConf( Streams = noMB )

dstWriter = SelDSTWriter("MyDSTWriter",
	SelectionSequences = sc.activeStreams(),
        OutputPrefix = 'Strip',
	OutputFileSuffix = '000000'
        )

from StrippingSelections.StartupOptions import veloNZSKiller, redoPV
dvinit = GaudiSequencer("DaVinciInitSeq")
dvinit.Members.insert(0, redoPV() )
dvinit.Members.insert(0, veloNZSKiller() )

DaVinci().EvtMax = 100                         # Number of events
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )

