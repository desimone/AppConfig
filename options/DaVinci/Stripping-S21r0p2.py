'''Global configuration for Stripping21r0p2.'''

from Configurables import Stripping

Stripping().Version = 'Stripping21r0p2'
Stripping().TCK = 0x39162102
#Stripping().MaxCombinations = 10000000
Stripping().MaxCandidates = 2000
