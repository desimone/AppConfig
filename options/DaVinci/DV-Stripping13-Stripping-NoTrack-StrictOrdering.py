"""
Options for building Stripping13 with strict ordering
of streams such that the micro-DSTs come last.

This is a temporary patch, see :
https://savannah.cern.ch/bugs/index.php?81294
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingSelections.Utils import buildStream, cloneLinesFromStream 

config  = strippingConfiguration('stripping13')
streams = []

streams.append( buildStream(config,'Dimuon') )
streams.append( buildStream(config,'Dielectron') )
streams.append( buildStream(config,'Semileptonic') )
streams.append( buildStream(config,'Bhadron') )
streams.append( buildStream(config,'CharmControl') )
streams.append( buildStream(config,'EW') )
streams.append( buildStream(config,'Radiative') )
streams.append( buildStream(config,'MiniBias') )

_leptonic       = buildStream(config,'Leptonic')
_charm_complete = buildStream(config,'CharmCompleteEvent')
_charm_micro    = buildStream(config,'Charm')
_calibration    = buildStream(config,'Calibration') 

#
# Now do some cloning to deal with the charm full/microDST streams
#

_charm_complete.appendLines( cloneLinesFromStream( _charm_micro, 'CompleteEvent', prescale = 0.02 ) )
_calibration.lines[:] = [ x for x in _calibration.lines if 'TrackEff' not in x.name() ]

streams.append( _calibration )
streams.append( _charm_complete )
streams.append( _charm_micro )
streams.append( _leptonic )

from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = streams,
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents )

from DSTWriters.__dev__.microdstelements import *
from DSTWriters.__dev__.Configuration import (SelDSTWriter,
                                              stripDSTStreamConf,
                                              stripDSTElements,
                                              stripMicroDSTStreamConf,
                                              stripMicroDSTElements)

#
# Configuration of MicroDST
#
mdstStreamConf = stripMicroDSTStreamConf()
mdstElements = stripMicroDSTElements()

#
# Configuration of Calibration stream 
# to include additional track containers
#
copyExtraTrackData = MoveObjects(objects = [ "Rec/TrackEffMuonTT_SelMuonTTPParts/ProtoParticles",
                                             "Rec/TrackEffMuonTT_SelMakeMuonTT/Tracks",
					     "Rec/Downstream/Tracks",
					     "Rec/Downstream/FittedTracks",
					     "Rec/ProtoP/DownMuonTrackEffDownMuonNominalProtoPMaker/ProtoParticles",
					     "Rec/VeloMuon/Tracks",
					     "Rec/ProtoP/VeloMuonTrackEffVeloMuonProtoPMaker/ProtoParticles" ])				 

calibElements = stripDSTElements() + [ copyExtraTrackData ]

leptonicMicroDSTname   = 'Leptonic'
charmMicroDSTname      = 'Charm'
calibrationFullDSTname = 'Calibration'

#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'              : stripDSTElements(),
    charmMicroDSTname      : mdstElements,
    leptonicMicroDSTname   : mdstElements,
    calibrationFullDSTname : calibElements
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf(),
    charmMicroDSTname      : mdstStreamConf,
    leptonicMicroDSTname   : mdstStreamConf,
    calibrationFullDSTname : stripDSTStreamConf()
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'SDST'
DaVinci().DataType = "2011"
DaVinci().EvtMax = 1000                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )



