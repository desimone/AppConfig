'''Global configuration for Stripping24r2.'''

from Configurables import Stripping

Stripping().Version = 'Stripping24r2'
Stripping().TCK = 0x44105242
#Stripping().MaxCombinations = 10000000
Stripping().MaxCandidates = 2000
