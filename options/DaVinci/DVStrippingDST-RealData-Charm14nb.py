from Gaudi.Configuration import *

from Configurables import SelDSTWriter, DaVinci

from StrippingConf.Configuration import StrippingConf


# Charm stream definition for 14nb-1 restripping
#


from StrippingConf.StrippingStream import StrippingStream
stream = StrippingStream("Charm")

# Lines for RICH PID

from StrippingSelections.StrippingInclPhi import InclPhiConf
MyPhiConfiguration=InclPhiConf.config_Sept2010 
MyPhiConfiguration['HighPtPrescale'] = 1
MyPhiConfiguration['LowPtPrescale'] = 0.5
MyPhiConf =  InclPhiConf("InclPhi", MyPhiConfiguration ) 
stream.appendLines( MyPhiConf.lines )

from StrippingSelections.StrippingV0ForPID import StrippingV0ForPIDConf
StrippingV0ForPIDConf().K0SBin1_LLPrescale = '0.103'
StrippingV0ForPIDConf().K0SBin2_LLPrescale = '0.155'
StrippingV0ForPIDConf().K0SBin3_LLPrescale = '0.620'

StrippingV0ForPIDConf().Lambda0Bin1_LLPrescale = '1.0'
StrippingV0ForPIDConf().Lambda0Bin2_LLPrescale = '1.0'
StrippingV0ForPIDConf().Lambda0Bin3_LLPrescale = '1.0'

StrippingV0ForPIDConf().VertexChi2 = '25'
StrippingV0ForPIDConf().LTimeFitChi2 = '36'

stream.appendLines( StrippingV0ForPIDConf().K0S_LL_Lines() )
stream.appendLines( StrippingV0ForPIDConf().Lam0_LL_Lines() )

# D / Ds -> hhh lines
from StrippingSelections import StrippingD2hhhXS
stream.appendLines( [
    StrippingD2hhhXS.lineD2KPP_B_LoosePID_BkgXS
  , StrippingD2hhhXS.lineD2KPP_B_LoosePID_SigXS
  , StrippingD2hhhXS.lineD2KKP_B_LoosePID_BkgXS
  , StrippingD2hhhXS.lineD2KKP_B_LoosePID_SigXS
  ] )


from StrippingSelections.StrippingDs2piPhiNoPt import StrippingDs2piPhiConf
stream.appendLines( [ StrippingDs2piPhiConf().line() ] )

# Lambda_c lines
from StrippingSelections.StrippingLambdac      import StrippingLambdacConf
stream.appendLines( StrippingLambdacConf().lines() )
 
# D*/D0 -> hh lines
from StrippingSelections.StrippingD2hhNoPIDXsec import StrippingD2hhNoPIDXsecConf
stream.appendLines( StrippingD2hhNoPIDXsecConf().lines() )

from StrippingSelections import StrippingDstarPromptWithD02HHNoPt
stream.appendLines( [
    StrippingDstarPromptWithD02HHNoPt.lineD02HHPromptNoPt
  , StrippingDstarPromptWithD02HHNoPt.lineDstarPromptWithD02HHNoPt
  ] )

# Two-body lines 
from StrippingSelections.StrippingDstarD02xxForCross import StrippingDstarD02xxForCrossConf
stream.appendLines( StrippingDstarD02xxForCrossConf().lines() )

# 4-body lines 
from StrippingSelections.StrippingD02K3Pi import StrippingD02K3PiForXSectionConf 
k3pi_config = StrippingD02K3PiForXSectionConf._default_config
k3pi_config['D0WideMassWin']=75
k3pi_config['D0DaughterPt']=0.25
k3pi_config['DstarDOCA']=0.5
k3pi_config['D0MaxDOCALoose']=0.5
k3pi_config['D0MaxDOCATight']=0.5
k3pi_config['D0DaughterIPchi2Loose']=1
k3pi_config['D0DaughterIPchi2Tight']=1
k3pi_config['D0FDchi2Loose']=16
k3pi_config['D0FDchi2Tight']=16
k3pi_config['TrackChi2DOF']=5
k3pi_config['DstarVtxChi2DOF']=100
D02K3Pi = StrippingD02K3PiForXSectionConf(config=k3pi_config)
stream.appendLines( [ D02K3Pi.wide_line_tagged, D02K3Pi.wide_line_untagged ] )

from StrippingSelections import StrippingDKPiGeo
stream.appendLines( [ StrippingDKPiGeo.line ] )

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Stripping job configuration
#

from Configurables import  ProcStatusCheck
filterBadEvents =  ProcStatusCheck()

sc = StrippingConf( Streams = [ stream ],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents )


dstWriter = SelDSTWriter( "MyDSTWriter",
                          SelectionSequences = sc.activeStreams(),
                          OutputFileSuffix = '000000'
                          )


DaVinci().EvtMax = 10000                        # Number of events
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )

EventSelector().Input   = [ "   DATAFILE='~/w0/Brunel.dst' TYP='POOL_ROOTTREE' OPT='READ'" ]

