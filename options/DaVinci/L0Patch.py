from Configurables import L0DUFromRawAlg, L0DUFromRawTool
l0du = L0DUFromRawAlg('L0DUFromRaw')
l0du.addTool(L0DUFromRawTool, name = 'L0DUFromRawTool')
l0du.L0DUFromRawTool.RawLocation = "pRec/RawEvent"
