"""
Options for building Stripping22. 
"""

#use CommonParticlesArchive
stripping='stripping22'
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
tmpconfig  = strippingConfiguration(stripping)

config={}

from copy import deepcopy

#change GEC for B2DMuNuX
for k,i in tmpconfig.iteritems():
    config[k]=i
    if k=='B2DMuNuX':
        ((config[k])['CONFIG'])['GEC_nLongTrk']=100000

#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)

#add line for ridge
from StrippingConf.StrippingLine import StrippingLine
rl=StrippingLine( "HighVeloMultLine",
                   prescale=1.0,
                   checkPV=False,
                   HLT1 = "HLT_PASS_RE('^Hlt1(HighVeloMult|HighVeloMultSinglePV)Decision$')",
                   HLT2 = "HLT_PASS_RE('Hlt2PassThroughDecision')")


for stream in streams:
    stream.lines=[l for l in stream.lines if (("TrackEffDown" not in l.name()))] #fix for Z02mumu bug

AllStreams = StrippingStream("AllStreams")

for stream in streams:
    if stream.name()=='ALL':
        AllStreams.appendLines(stream.lines)
    if stream.name()=='ALLTURBO':
        for line in stream.lines:
            if line not in AllStreams.lines:
                AllStreams.appendLines([line])

AllStreams.appendLines([rl])

sc = StrippingConf( Streams = [AllStreams],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip' )

AllStreams.sequence().IgnoreFilterPassed = True

enablePacking = True

from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }


SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking, selectiveRawEvent=True) }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x38002200)

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().EvtMax = -1 # Number of events
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
