from Gaudi.Configuration import *

from Configurables import SelDSTWriter, DaVinci

from StrippingConf.Configuration import StrippingConf

from StrippingSettings.Sept2010 import StreamCalibration
from StrippingSettings.Sept2010 import StreamBhadron
from StrippingSettings.Sept2010 import StreamCharm
from StrippingSettings.Sept2010 import StreamDielectron
from StrippingSettings.Sept2010 import StreamDimuon
from StrippingSettings.Sept2010 import StreamMiniBias
from StrippingSettings.Sept2010 import StreamSemileptonic
from StrippingSettings.Sept2010 import StreamRadiative
from StrippingSettings.Sept2010 import StreamEW


allStreams = [
               StreamCalibration.stream, 
               StreamBhadron.stream,
               StreamCharm.stream,
               StreamDielectron.stream,
               StreamDimuon.stream,
               StreamMiniBias.stream,
               StreamSemileptonic.stream,
               StreamRadiative.stream,
               StreamEW.stream,
             ]


MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Stripping job configuration
#

from Configurables import  ProcStatusCheck
filterBadEvents =  ProcStatusCheck()

sc = StrippingConf( Streams = allStreams,
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents )


dstWriter = SelDSTWriter( "MyDSTWriter",
                          SelectionSequences = sc.activeStreams(),
                          OutputFileSuffix = '000000'
                          )


DaVinci().EvtMax = 10000                        # Number of events
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )

EventSelector().Input   = [ "   DATAFILE='~/w0/Brunel.dst' TYP='POOL_ROOTTREE' OPT='READ'" ]

