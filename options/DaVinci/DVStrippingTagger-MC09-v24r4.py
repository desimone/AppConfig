#
#   Stripping selections job (SETC output)
#
#   @author J. Palacios/ A. Poluektov
#   @date 2009-11-05
#

from Gaudi.Configuration import *

from StrippingConf.Configuration import StrippingConf

from StrippingSelections import StreamBmuon, StreamHadron, StreamJpsi, StreamDstar, StreamLambda, StreamBelectron

sc = StrippingConf( Streams = [ StreamBmuon.stream
                    , StreamHadron.stream 
                    , StreamJpsi.stream
                    , StreamDstar.stream
                    , StreamLambda.stream
#                    , StreamBelectron.stream 
                    ], TES = True )

from Configurables import EventTuple, TupleToolSelResults

tag = EventTuple("TagCreator")
tag.EvtColsProduce = True
tag.ToolList = [ "TupleToolEventInfo", "TupleToolRecoStats", "TupleToolSelResults"  ]
tag.addTool(TupleToolSelResults)

tag.TupleToolSelResults.Selections = sc.selections()

from Configurables import DaVinci

DaVinci().appendToMainSequence( [ sc.sequence() ] )   # Append the stripping selection sequence to DaVinci
DaVinci().appendToMainSequence( [ tag ] )             # Append the TagCreator to DaVinci
DaVinci().DataType = "MC09"
DaVinci().ETCFile = "DVStripping_SETC.root"

# DaVinci().EvtMax = 1000
#
# # Tagger should take the DST produced by the DVStrippingDST as input: 
# 
# EventSelector().Input   = [
# "   DATAFILE='Sel.Hadron.dst' TYP='POOL_ROOTTREE' OPT='READ'"
# ]
