#
#   Stripping selections job (DST output)
#
#   @author J. Palacios/A. Poluektov
#   @date 2009-11-05
#

from Gaudi.Configuration import *
from Configurables import SelDSTWriter, DaVinci

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"


from StrippingConf.Configuration import StrippingConf
from StrippingSelections.Streams import allStreams
from StrippingSelections import StreamMiniBias, StreamV0

noMB = allStreams
noMB.remove( StreamMiniBias.stream )
noMB.remove( StreamV0.stream )
sc = StrippingConf( Streams = noMB )

# Dirac should modify OutputFilePrefix.
# SelDSTWriter("StripMC09DSTWriter").OutputFileSuffix = '012345'
dstWriter = SelDSTWriter("StripMC09DSTWriter",
                         SelectionSequences = sc.activeStreams(),
                         OutputPrefix = 'Strip',
                         OutputFileSuffix = '000000' ,
                         ExtraItems = ['/Event/DAQ/RawEvent'],
                         WriteFSR = False 
                         )

DaVinci().DataType = "MC09"                   # Default is "MC09"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().WriteFSR = False
DaVinci().Lumi = False

DaVinci().EvtMax = 1000
