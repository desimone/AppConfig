#
#   Stripping selections job (ETC output)
#
#   @author J. Palacios
#   @date 2009-09-16
#

from Gaudi.Configuration import *
from Configurables import DaVinci
from StrippingConf.Configuration import StrippingConf

StrippingConf().OutputType = "ETC"

DaVinci().DataType = "MC09"
DaVinci().ETCFile = "DVStripping_ETC.root"
DaVinci().HistogramFile = "DVStripping_Hist.root"
