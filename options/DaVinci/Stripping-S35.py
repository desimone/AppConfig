'''Global configuration for Stripping35.'''

from Configurables import Stripping, TrackSys

TrackSys().GlobalCuts = { 'Velo':20000, 'IT':999999, 'OT':999999 }

Stripping().Version = 'Stripping35'
Stripping().TCK = 0x44703500
Stripping().MaxCandidates = 2000
