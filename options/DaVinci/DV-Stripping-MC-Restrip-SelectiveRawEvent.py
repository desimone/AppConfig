# Restripping options for full DST, unfiltered MC that has been 
# produced with SelectiveRawEvent = True.
# !!! Must be added *after* the standard stripping options
# in the list of job options !!!

# Remove all RequiredRawEvents from stripping lines, and some
# RelatedInfos that use raw banks. Also disable writing of 
# track clusters, since these can't be recovered. For this
# reason, also remove all TrackEff lines.
# Consequently, the output will contain no raw banks and
# no persisted clusters, so track refitting isn't possible.

from DSTWriters.Configuration import SelDSTWriter
from Configurables import AddRelatedInfo, GaudiSequencer
from DSTWriters.microdstelements import PackTrackingClusters
from Configurables import EventNodeKiller, DaVinci

# Kill all raw banks up front so we don't have a subset of events
# that do have raw banks.
rawkiller = EventNodeKiller('RawKiller')
rawkiller.Nodes = ['/Event/' + det + '/RawEvent' for det in 
                   ('Calo', 'HC', 'Muon', 'Rich', 'Tracker', 'Velo', 'Other')]
DaVinci().prependToMainSequence([rawkiller])

# Remove RelatedInfos that use raw banks.
# RelInfoVeloTrackMatch doesn't actually access the raw banks itself, 
# but requires VELO tracks to be rebuilt, which can't be done.
killrelinfos = ('RelInfoMuonIDPlus', 'RelInfoMuonIsolation', 'RelInfoVeloTrackMatch')
dstWriter = SelDSTWriter('MyDSTWriter')
for stream in dstWriter.SelectionSequences :
    for line in stream.lines :
        line.RequiredRawEvents = None
        if line.RelatedInfoTools :
            line._configurable.Filter1.Members = \
                filter(lambda alg : (not isinstance(alg, AddRelatedInfo) or 
                                     not any(relinfo in alg.Tool for relinfo in killrelinfos)),
                       line._configurable.Filter1.Members)
            # These aren't strictly necessary, except for bookkeeping.
            line.RelatedInfoTools = filter(lambda relinfo : relinfo['Type'] not in killrelinfos,
                                           line.RelatedInfoTools)
            line._members = line._configurable.Filter1.Members

# Remove packing of track clusters.
dstWriter.MicroDSTElements['default'] = filter(lambda alg : not isinstance(alg, PackTrackingClusters),
                                               dstWriter.MicroDSTElements['default'])

# Remove lines that use raw banks.
killlines = ('TrackEff',)
for seqname in 'StrippingSequenceStreamAllStreams', 'StrippingStreamSeqAllStreams' :
    linesseq = GaudiSequencer(seqname)
    linesseq.Members = filter(lambda alg : not any(killline in alg.name() for killline in killlines),
                              linesseq.Members)
