'''Global configuration for Stripping29.'''

from Configurables import Stripping

Stripping().Version = 'Stripping29'
Stripping().TCK = 0x42502900
Stripping().MaxCombinations = 10000000
Stripping().MaxCandidates = 2000
