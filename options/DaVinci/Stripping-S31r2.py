'''Global configuration for Stripping31r2.'''

from Configurables import Stripping, TrackSys

TrackSys().GlobalCuts = { 'Velo':20000, 'IT':999999, 'OT':999999 }

Stripping().Version = 'Stripping31r2'
Stripping().TCK = 0x41503120
Stripping().MaxCandidates = 2000
