#######################################################################
#
# PassLinesPrescales. Reduce MB to 1%
#
# @author P. Koppenburg
# @date 10/05/2010
#
from Configurables import DeterministicPrescaler
DeterministicPrescaler("StrippingMBMicroBiasPreScaler").AcceptFraction = 0.01
DeterministicPrescaler("StrippingMBNoBiasPreScaler").AcceptFraction = 0.01
DeterministicPrescaler("StrippingHlt1L0AnyPreScaler").AcceptFraction = 0.01

