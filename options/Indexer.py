from Gaudi.Configuration import importOptions

from warnings import warn
warn("obsolete option file, use '$APPCONFIGOPTS/Noether/Indexer.py' instead")

importOptions('$APPCONFIGOPTS/Noether/Indexer.py')
