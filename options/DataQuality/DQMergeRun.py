from __future__ import print_function
from Gaudi.Configuration import *

import re
import xml.dom.minidom
import sys

import ROOT

from   ROOT import gDirectory, gROOT
from   ROOT import kTRUE, kFALSE
from   ROOT import TFile, TList, TIter, TKey
from   ROOT import TDirectory, TChain
from   ROOT import TH1, TH1F
from   ROOT import TMath
from   ROOT import SetOwnership


FILE_COUNT     = 0
SPLIT_BY       = 20

ALL_HIST_NAMES = {}
ALL_TREE_NAMES = {}

#-------------------------------------------------------------------------------
def AddH1(obj, target, sourcelist, curr_idx, path):
    """ 
         \author  M. Adinolfi
         \version 1.0
    """    

    hr = 0
    hm = 0
    he = 0

    #
    # Skip if already merged
    #
    
    name     = obj.GetName()
    fullname = target.GetPathStatic() + '/' + name
    if fullname in ALL_HIST_NAMES:
        return None
    ALL_HIST_NAMES[fullname] = 1

    #
    # Make sure to create the trend plots in the target output directory
    #

    target.cd()

    for next_idx, nextSource in enumerate(sourcelist[curr_idx+1:], curr_idx+1):
        nextDir = nextSource.GetDirectory(path)
        if nextDir:
            nextDir.cd()
            key2 = nextDir.GetListOfKeys().FindObject(name)

            if not key2:
                continue
            
            h2 = key2.ReadObj()
            SetOwnership(h2, True)
            obj.Add(h2)

            h2.SetDirectory(0)
            h2.Delete()
            del h2

    target.cd()
    obj = WriteAndRemoveObject(obj, target)
            
    return None
#-------------------------------------------------------------------------------
def AddTree(obj, target, sourcelist, curr_idx, path):
    """ 
         \author  M. Adinolfi
         \version 1.0
    """    

    
    #
    # Skip if already merged
    #
    
    name     = obj.GetName()
    fullname = target.GetPathStatic() + '/' + name
    if fullname in ALL_TREE_NAMES:
        return None
    ALL_TREE_NAMES[fullname] = 1

    fullpath = path + '/' + name

    #
    # Make sure to create the trend plots in the target output directory
    #

    target.cd()

    globalChain = TChain(fullpath)

    current = sourcelist[curr_idx]

    globalChain.AddFile(current.GetName())
    
    for next_idx, nextSource in enumerate(sourcelist[curr_idx+1:], curr_idx+1):
        globalChain.AddFile(nextSource.GetName())

    target.cd()

    obj = WriteAndRemoveObject(globalChain, target)
    
    return None
#-------------------------------------------------------------------------------
def DQMergeRun():
    """ 
         \author  M. Adinolfi
         \version 1.0
    """
    from ProdConf import ProdConf

    pc  = ProdConf()
    res = ReadXMLFileCatalog(pc.XMLFileCatalog)

    if not res['OK']:
        print('Could not parse XML file catalog %s' %(pc.XMLFileCatalog))
        exit(1)

    outFileName = '%s.%s' %(pc.OutputFilePrefix,
                            pc.OutputFileTypes[0])

    res = SortFiles(pc, res['ID'])

    if res['OK']:
        allFiles = res['allFiles']
        res      = Merge(allFiles, outFileName)

    WriteSummary(pc, allFiles, outFileName)
    
    print('exit')
    exit(0);

#-------------------------------------------------------------------------------
def FileClose(f):
    """ 
         \author  M. Adinolfi
         \version 1.0
    """
    print('Closing ', f.GetName())
    f.Close()
    f.Delete()
    gROOT.GetListOfFiles().Remove(f)
    del f

    return None

#-------------------------------------------------------------------------------
def FileOpen(filename):
    """ 
         \author  M. Adinolfi
         \version 1.0
    """
    print('Adding ', filename)

    fs = TFile.Open(filename, "read")
    if not fs:
        print('Cannot open file %s' %(filename))
        exit(5)
    
    if fs.IsZombie():
        print('Cannot open file %s' %(filename))
        exit(4)
    SetOwnership(fs, True)
    
    return fs        
#-------------------------------------------------------------------------------
def Merge(allFiles, outFileName):
    """ 
         \author  M. Adinolfi
         \version 1.0
    """

    global FILE_COUNT
    FILE_COUNT = 0

    if len(allFiles) > 1:
        while len(allFiles) > 1:
            fType = 'Temp'
            if len(allFiles) <= SPLIT_BY:
                fType = outFileName
            allFiles = MergeFiles(fType, allFiles)
    else:
        allFiles = MergeFiles(outFileName, allFiles)
    
    return
#-------------------------------------------------------------------------------
def MergeFiles(ftype, fileList):
    """ 
         \author  M. Adinolfi
         \version 1.0
    """
    global FILE_COUNT
    
    nstep = 1

    if len(fileList) > SPLIT_BY :
        nstep = int(len(fileList)/SPLIT_BY) + 1
        
    outfiles = []    
    for i in range(0, nstep):
        first = i     * SPLIT_BY
        last  = (i+1) * SPLIT_BY

        if last > len(fileList):
            last = len(fileList)
            
        mergeList = []
        for j in range(first, last):
            mergeList.append(fileList[j])
            
        if not len(mergeList):
            break

        if re.search('\.root$', ftype):
            target = ftype
        else:
            target = '%s_%s.root' %(ftype, str(FILE_COUNT))

        MergeSplit(target, mergeList)

        FILE_COUNT += 1
        outfiles.append(target)

    return outfiles

#-------------------------------------------------------------------------------
def MergeRootFileExt(target, sourcelist, start):
    """ 
         \author  M. Adinolfi
         \version 1.0
    """
    global ALL_HIST_NAMES

    path = target.GetPath()
    m    = re.search('(?<=:/).*', path)
    path = m.group(0)

    for curr_idx, fs in enumerate(sourcelist[start:], start):
        current = fs.GetDirectory(path)
        if not current:
            continue
        SetOwnership(current, True)
        
        status = TH1.AddDirectory
        TH1.AddDirectory = False

        for key in current.GetListOfKeys():            
            current.cd()

            obj  = key.ReadObj()
            SetOwnership(obj, True)

            if obj.IsA().InheritsFrom("TH1"):
                obj = AddH1(obj, target, sourcelist, curr_idx, path)
            elif obj.IsA().InheritsFrom("TTree"):
                obj = AddTree(obj, target, sourcelist, curr_idx, path)
            elif obj.IsA().InheritsFrom("TDirectory"):
                targetDir = target.FindKey(obj.GetName())

                if targetDir:
                    continue

                target.cd()
                newdir = target.mkdir(obj.GetName(), obj.GetTitle())

                SetOwnership(newdir, True)
                
                print('Merging ', newdir.GetPathStatic())
                MergeRootFileInt(newdir, sourcelist, curr_idx)
                obj = WriteAndRemoveObject(obj, target)
            else:
                continue
            
    target.SaveSelf(True)
    TH1.AddDirectory = status

    return

#-------------------------------------------------------------------------------
def MergeRootFileInt(target, sourcelist, start):
    """ 
         \author  M. Adinolfi
         \version 1.0
    """
    path = target.GetPath()
    m    = re.search('(?<=:/).*', path)
    path = m.group(0)

    for curr_idx, fs in enumerate(sourcelist[start:], start):
        current = fs.GetDirectory(path)
        if not current:
            continue
        SetOwnership(current, True)
        
        status = TH1.AddDirectory
        TH1.AddDirectory = False

        for key in current.GetListOfKeys():            
            current.cd()

            obj  = key.ReadObj()
            SetOwnership(obj, True)

            if obj.IsA().InheritsFrom("TH1"):
                obj = AddH1(obj, target, sourcelist, curr_idx, path)                    
            elif obj.IsA().InheritsFrom("TTree"):
                obj = AddTree(obj, target, sourcelist, curr_idx, path)
            elif obj.IsA().InheritsFrom("TDirectory"):
                targetDir = target.FindKey(obj.GetName())
                if not targetDir:
                    target.cd()
                    newdir = target.mkdir(obj.GetName(), obj.GetTitle())
                else:
                    newdir = target.GetDirectory(obj.GetName())
                SetOwnership(newdir, True)
                
                print('Merging ', newdir.GetPathStatic())
                MergeRootFileExt(newdir, sourcelist, curr_idx)
                obj = WriteAndRemoveObject(obj, target)
            else:
                continue
            
    target.SaveSelf(True)
    TH1.AddDirectory = status

    return
    
#-------------------------------------------------------------------------------
def MergeSplit(targetName, files):
    """ 
         \author  M. Adinolfi
         \version 1.0
    """
    print('Opening ', targetName)
    target = TFile()
    target = TFile.Open(targetName, "RECREATE")
    
    SetOwnership(target, True)
    
    filelist = map(FileOpen, files)

    MergeRootFileExt(target, filelist, 0)
    
    print('Writing ', target.GetName())
    target.Write()
    
    map(FileClose, filelist)
    FileClose(target)
    
    del filelist[:]

    return    

#-------------------------------------------------------------------------------
def ReadXMLFileCatalog(xmlfile):
    """ 
         \author  M. Adinolfi
         \version 1.0
    """
    ret = {'OK' : True,
           'ID' : {}
           }

    sfile = file(xmlfile, 'r')

    try:
        dom      = xml.dom.minidom.parse(xmlfile)
        catalog  = dom.getElementsByTagName('POOLFILECATALOG')[0]
        allFiles = catalog.getElementsByTagName('File')

        for thisFile in allFiles:
            fileId  = thisFile.getAttribute('ID')
            if fileId not in ret['ID']:
                ret['ID'][fileId] = {'lfn' : None,
                                     'pfn' : None}

            phys    = thisFile.getElementsByTagName('physical')
            logical = thisFile.getElementsByTagName('logical')

            for p in phys:
                pfns  = p.getElementsByTagName('pfn')
                for pfn in pfns:
                    name = pfn.getAttribute('name')
                    ret['ID'][fileId]['pfn'] = name

            for l in logical:
                lfns  = l.getElementsByTagName('lfn')
                for lfn in lfns:
                    name = lfn.getAttribute('name')
                    ret['ID'][fileId]['lfn'] = name
                
    except:
        ret['OK'] = False

    return ret

#-------------------------------------------------------------------------------
def SortFiles(pc, fileIds):
    """  Sort the list of input LFN and divide them between BRUNEL and
         DAVINCI files.

         \author  M. Adinolfi
         \version 1.0
    """
    retval = {'OK'       : True,
              'allFiles' : []}

    global brunelFile
    global davinciFile
    global otherFile
    global nInFile

    brunelFile  = {}
    davinciFile = {}
    otherFile   = {}
    
    for inputFile in pc.InputFiles:
        if re.search('^LFN:', inputFile):
            ip    = re.sub('LFN:', '', inputFile)
            found = False

            for i in fileIds:
                if fileIds[i]['lfn'] == ip:
                    found   = True
                    keyName = None

                    pfn = fileIds[i]['pfn']
                    
                    retval['allFiles'].append(pfn)
            if not found:
                print('Cannot find input file: %s' %ip)
                exit(2)
        else:
            
            retval['allFiles'].append(inputFile)
    
    return retval
#-------------------------------------------------------------------------------
def WriteAndRemoveObject(obj, target):
    """  
         \author  M. Adinolfi
         \version 1.0
    """

    if obj.IsA().InheritsFrom("TTree"):
        obj.Merge(target.GetFile(), 0, 'keep')
    else:
        obj.Write(obj.GetName())

        obj.Delete()
        del obj
    
    return None

#-------------------------------------------------------------------------------
def WriteSummary(pc, allFiles, outFileName):
    """  
         \author  M. Adinolfi
         \version 1.0
    """

    f = open(pc.XMLSummaryFile, 'w')

    f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
    f.write('<summary xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.0" xsi:noNamespaceSchemaLocation="$XMLSUMMARYBASEROOT/xml/XMLSummary.xsd">\n')
    f.write('    <success>True</success>\n')
    f.write('    <step>finalize</step>\n')
    f.write('    <usage><stat unit="KB" useOf="MemoryMaximum">123456</stat></usage>\n')
    f.write('    <input>\n')
    
    c = 0
    for inputFile in allFiles:
        inFile = re.sub('.*/', '', inputFile)
        outmess = '     <file GUID="" name="PFN:%s" status="full">1</file>\n' %(inFile)
        f.write(outmess)
        c += 1
        
    f.write('    </input>\n')
    f.write('    <output>\n')
    
    outmess = '     <file GUID="" name="PFN:%s" status="full">%s</file>\n' %(outFileName, str(c))
    f.write(outmess)
    f.write('    </output>\n')
    f.write('</summary>\n')

    f.close()
    
    return

# MAIN is here -----------------------------------------------------------------

TFile.Open._creates = True

if __name__ == '__main__':
    from GaudiKernel.Configurable import applyConfigurableUsers
    from Gaudi.Configuration import importOptions
    map(importOptions, sys.argv[1:])
    applyConfigurableUsers()
    DQMergeRun()
else:
    appendPostConfigAction(DQMergeRun)
    
