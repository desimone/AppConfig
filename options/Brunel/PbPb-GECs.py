# option file for reconstrucion of lead-lead  data
# These GECS (in TrackSys and RichRecSysConf) will essentially
# disable the application of Global Event Cuts in the track reconstruction
# and Rich reconstruction.  
from Configurables import TrackSys

TrackSys().GlobalCuts = { 'Velo':20000, 'IT':999999, 'OT':999999 }

from Configurables import RichRecSysConf
rConf = RichRecSysConf("RichOfflineRec")
#BigNumber = 999999
rConf.pixelConfig().MaxPixels      = 90000
rConf.trackConfig().MaxInputTracks = 99999
rConf.trackConfig().MaxUsedTracks  = 10000 # this used to be 1000
rConf.photonConfig().MaxPhotons    = 900000 # this used to be 250000
rConf.gpidConfig().MaxUsedPixels   = 120000 # this used to be 30000
rConf.gpidConfig().MaxUsedTracks   = 10000 # this used to be 1000

