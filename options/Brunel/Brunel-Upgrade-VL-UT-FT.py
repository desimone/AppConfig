## ############################################################################
## # File for running Brunel with Upgrade database
## #
## # Upgrade Detectors: VL UT FT Rich1Pmt Rich2Pmt
## #
## # Syntax is:
## #   gaudirun.py Brunel-Upgrade-VL-UT-FT.py <someInputJobConfiguration>.py
## ############################################################################

from Gaudi.Configuration import *
from Configurables import CondDB

#CondDB().Upgrade     = True
#CondDB().AllLocalTagsByDataType=["VL+UT","FT"]

from Configurables import Brunel
Brunel().Detectors = ['VL', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Spd', 'Prs', 'Ecal', 'Hcal', 'Muon', 'Magnet', 'Tr' ]

