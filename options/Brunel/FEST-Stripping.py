##############################################################################
# File for running Brunel on real data in default stripping workflow,
# (real data, field on, default database tags) starting from strip ETC
# Syntax is:
#   gaudirun.py Brunel-Stripping.py <someDataFiles>.py
##############################################################################

from Gaudi.Configuration import *
from Configurables import Brunel, LHCbApp

Brunel().InputType  = "ETC"
# Brunel().NoWarnings = True   # This gets configuration error in RichRecQC
Brunel().Simulation = False

from Configurables import  TagCollectionSvc
ApplicationMgr().ExtSvc  += [ TagCollectionSvc("EvtTupleSvc") ]

###############################################################################
#
# All wah't below to be overwritten by Dirac
#
###############################################################################
#
# ETC filename has to be set by Dirac, somehow
#
ETC_filename = "DVStripping_ETC.root"
EventSelector().Input   = [
    "COLLECTION='TagCreator/EventTuple' DATAFILE='"+ETC_filename+"' TYP='POOL_ROOT' SEL='(StrippingGlobal==1)'" ]

#
# Set database and file catalog from Dirac
#
FileCatalog().Catalogs = [ "xmlcatalog_file:FEST-4867.xml" ]
LHCbApp().DDDBtag   = "MC09-20090602"
LHCbApp().CondDBtag = "head-20090508" 
