# options to loosen PV fitting for SMOG data
from Gaudi.Configuration import *

def changePVCuts():
    from Configurables import PatPV3D, PVSeed3DTool, PVOfflineTool,LSAdaptPV3DFitter
    from GaudiKernel.SystemOfUnits import mm

    pvAlg = PatPV3D("PatPV3D")
    pvAlg.addTool(PVOfflineTool,"PVOfflineTool")
    pvAlg.PVOfflineTool.UseBeamSpotRCut = True
    pvAlg.PVOfflineTool.BeamSpotRCut = 4.0

    pvAlg.PVOfflineTool.LSAdaptPV3DFitter.MinTracks = 3
    pvAlg.PVOfflineTool.LSAdaptPV3DFitter.trackMaxChi2 = 12.0

    pvseed = PVSeed3DTool()
    pvseed.zMaxSpread = 10.0*mm
    pvseed.MinCloseTracks = 3
    pvseed.TrackPairMaxDistance = 2.0*mm
    pvAlg.PVOfflineTool.addTool( pvseed )


from Gaudi.Configuration  import appendPostConfigAction
appendPostConfigAction( changePVCuts )
