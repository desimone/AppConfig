##############################################################################
# File for reconstructing the Spd threshold scan runs
##############################################################################

from Configurables import Brunel
Brunel().MainSequence = ['ProcessPhase/Reco']
Brunel().RecoSequence = ['Decoding', 'VELO', 'TT', 'IT', 'OT', 'TrHLT1', 'Vertex', 'TrHLT2', 'SUMMARY']
Brunel().Detectors = ['Velo', 'TT', 'IT', 'OT', 'Magnet']
Brunel().RawBanksToKill = ['HC', 'Muon', 'Rich']
Brunel().PhysFilterMask = [ ]
Brunel().VetoHltErrorEvents = False




