##############################################################################
# File for running Brunel on MC data with default Upgrade settings,
# reading and saving no MC Truth, except for pileup information
#
# Syntax is:
# gaudirun.py Brunel/Upgrade-NoTruth.py Conditions/<someTag>.py <someDataFiles>.py
##############################################################################

from Configurables import Brunel

Brunel().DataType  = "Upgrade"
Brunel().InputType = "DIGI" # implies also Brunel().Simulation = True
Brunel().DigiType  = "Minimal"

##############################################################################
