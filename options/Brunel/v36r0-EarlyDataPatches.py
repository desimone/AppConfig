# Options for reprocessing early data with Brunel v36r0

from Gaudi.Configuration import *

from Configurables import Brunel, PatDownstream, PatForward

# Standard early data tuning
Brunel().SpecialData += ["earlyData"]

# Increase maximum CK theta value for aerogel (will be in LHCb v29r1)
from Configurables import RichTools
RichTools().photonCreator().MaxAllowedCherenkovTheta = [ 0.320, 0.075, 0.035 ]

# New tuning of tracking
def patchTracking():
	PatDownstream().xPredTol2 = 20.0
	PatDownstream().TolMatch = 3.5
	PatDownstream().TolUV = 4.0
	PatDownstream().maxWindowSize = 25.0
	PatDownstream().MaxChisq  = 20.0
	PatDownstream().MaxDistance = 0.3
	PatDownstream().deltaP = 2.0
	PatDownstream().errorZMagnet = 30.0
	PatForward("PatForward").PatForwardTool.MinXPlanes = 4
	PatForward("PatForward").PatForwardTool.MinPlanes = 8
	PatForward("PatForward").PatForwardTool.MaxSpreadX = 1.5
	PatForward("PatForward").PatForwardTool.MaxSpreadY = 3.0
	PatForward("PatForward").PatForwardTool.MaxChi2 = 40
	PatForward("PatForward").PatForwardTool.MaxChi2Track = 40
	PatForward("PatForward").PatForwardTool.MinHits = 12
	PatForward("PatForward").PatForwardTool.MinOTHits = 14
appendPostConfigAction(patchTracking)
