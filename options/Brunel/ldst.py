# These options will set the flag necessary to have simulation dst with Boole linkers added

from Configurables import Brunel
from Gaudi.Configuration import *

Brunel().OutputType = 'LDST'

# Workaround for the fact the original DstConf settings excluded the RICH and MUON
# digit summaries from the LDST format.
def addDigSumsToLDST() :
    from Configurables import DstConf, DigiConf
    writer = OutputStream( DstConf().getProp("Writer") )
    if '/Event/pSim/Rich/DigitSummaries#1' not in writer.ItemList :
        DigiConf().addMCDigitSummaries(writer)
appendPostConfigAction(addDigSumsToLDST)
