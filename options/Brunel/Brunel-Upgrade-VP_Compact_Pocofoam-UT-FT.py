## ############################################################################
## # File for running Brunel with Upgrade database
## #
## # Upgrade Detectors: VP_Compact_Pocofoam UT FT Rich1Pmt Rich2Pmt
## #
## # Syntax is:
## #   gaudirun.py Brunel-Upgrade-VP_Compact_Pocofoam-UT-FT.py <someInputJobConfiguration>.py
## ############################################################################

from Gaudi.Configuration import *
from Configurables import LHCbApp, CondDB

CondDB().Upgrade = True
CondDB().AllLocalTagsByDataType = ["VP_Compact_Pocofoam+UT", "FT"]

from Configurables import Brunel
Brunel().DataType     = "Upgrade" 

