from Configurables import Brunel

# no Hlt12/DecReports, so every event is an "error" => disable check
Brunel().VetoHltErrorEvents = False

# No need to set PhysFilterMask as no HLT routing bits bank present and
# HltRoutingBitsFilter's default behaviour is to pass in this case.
# We have these "ERRORS" though:
# PhysFilter        SUCCESS  #ERRORS     = 200      Message = 'Unexpected # of HltRoutingBits rawbanks'
