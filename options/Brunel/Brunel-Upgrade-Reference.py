## ############################################################################
## # File for running Brunel with Upgrade database
## #
## # Upgrade Detectors: VP UT FT Rich1Pmt Rich2Pmt
## #
## # Syntax is:
## #   gaudirun.py Brunel-Upgrade-VP-UT-FT.py <someInputJobConfiguration>.py
## ############################################################################

from Gaudi.Configuration import *
from Configurables import CondDB

CondDB().Upgrade     = True
#CondDB().AllLocalTagsByDataType=["VP+UT","FT"]

from Configurables import Brunel
Brunel().Detectors = ['Velo', 'PuVeto', 'Rich1Pmt', 'Rich2Pmt', 'TT', 'IT', 'OT', 'Spd', 'Prs', 'Ecal', 'Hcal', 'Muon', 'Magnet', 'Tr']


