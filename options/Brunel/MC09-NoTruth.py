##############################################################################
# File for running Brunel on MC data with default MC09 settings,
# reading and saving no MC Truth, except for pileup information
#
# Syntax is:
# gaudirun.py Brunel/MC09-NoTruth.py Conditions/<someTag>.py <someDataFiles>.py
##############################################################################

from Gaudi.Configuration import importOptions
from Configurables import Brunel

importOptions("$APPCONFIGOPTS/Brunel/MC-NoTruth.py")
importOptions("$APPCONFIGOPTS/Brunel/DataType-MC09.py")

##############################################################################
