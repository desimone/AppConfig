##############################################################################
# File for running Brunel with Upgrade configuration
##############################################################################

from Brunel.Configuration import *


from Configurables import LHCbApp, CondDB

CondDB().Upgrade = True

if "Calo_NoSPDPRS" in CondDB().AllLocalTagsByDataType:
    CondDB().AllLocalTagsByDataType.remove("Calo_NoSPDPRS")
 
from Configurables import Brunel
for det in ["Spd", "Prs"]:
    if det not in Brunel().Detectors:
        Brunel().Detectors.append(det)

Brunel().DataType = "Upgrade"

