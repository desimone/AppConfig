# option file for reconstrucion of lead-lead  data
# These GECS (in TrackSys and RichRecSysConf) will essentially
# disable the application of Global Event Cuts in the track reconstruction
# and Rich reconstruction.  

from Configurables import Brunel
Brunel().SpecialData += ["pA"]

from Configurables import TrackSys
TrackSys().GlobalCuts = { 'Velo':20000, 'IT':999999, 'OT':999999 }
