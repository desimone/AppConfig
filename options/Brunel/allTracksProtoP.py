# Select 'all' tracks to be made into ProtoParticles, useful in early data ...
from Configurables import  GlobalRecoConf
GlobalRecoConf().TrackTypes = [ "Long","Upstream","Downstream","Ttrack","Velo","VeloR" ]
GlobalRecoConf().TrackCuts  = { }
