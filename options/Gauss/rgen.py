# These options correspond to running only the generator phase of Gauss and
# writing MCParticles/MCVertices filled directly from
# the generator in .rgen (reduced generator) files, i.e. HepMC is not present
# in this file type

from Configurables import Gauss

Gauss().Phases = ['Generator', 'GenToMCTree']
Gauss().OutputType = 'RGEN'
