# File for setting Beam conditions for 2010 simulation
#
# Beam 3.5 TeV
#
# Syntax is: 
#  gaudirun.py $APPCONFIGOPTS/Gauss/Beam3500GeV-mu100-xangle340urad-nu2,5.py
#              $APPCONFIGOPTS/Conditions/XXXXXXX.py
#              $DECFILESROOT/options/30000000.opts (ie. event type)
#              $LBGENROOT/options/GEN.py (i.e. production engine)
#              gaudi_extra_options_NN_II.py (ie. job specific: random seed,
#                               output file names, see Gauss-Job.py as example)
#
from Gauss.Configuration import * 

#--Set the L/nbb, total cross section and revolution frequency and configure
#--the pileup tool
Gauss().Luminosity        = 0.308*(10**30)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().CrossingRate      = 11.245*SystemOfUnits.kilohertz
Gauss().TotalCrossSection = 91.1*SystemOfUnits.millibarn

#--Set the luminous region for colliding beams and beam gas and configure
#--the corresponding vertex smearing tools, the choice of the tools is done
#--by the event type
Gauss().InteractionSize = [ 0.040*SystemOfUnits.mm, 0.040*SystemOfUnits.mm,
                            5.00*SystemOfUnits.cm ]

Gauss().InteractionPosition = [ 0.0*SystemOfUnits.mm ,
                                0.0*SystemOfUnits.mm ,
                                5.0*SystemOfUnits.mm ]

Gauss().BeamSize        = [ 0.057*SystemOfUnits.mm, 0.057*SystemOfUnits.mm 
]

#--Set the energy of the beam,
#--the half effective crossing angle (in LHCb coordinate system),
#--beta* and emittance
Gauss().BeamMomentum      = 3.5*SystemOfUnits.TeV
Gauss().BeamCrossingAngle = 0.170*SystemOfUnits.mrad
Gauss().BeamEmittance     = 0.67*(10**(-9))*SystemOfUnits.rad*SystemOfUnits.m
Gauss().BeamBetaStar      = 3.5*SystemOfUnits.m
Gauss().BeamLineAngles    = [-0.09*SystemOfUnits.mrad, 0.025*SystemOfUnits.mrad]


#--Starting time, to identify beam conditions, all events will have the same
ec = EventClockSvc()
ec.addTool(FakeEventTime(), name="EventTimeDecoder")
ec.EventTimeDecoder.StartTime = 301.0*SystemOfUnits.ms
ec.EventTimeDecoder.TimeStep  = 0.0

