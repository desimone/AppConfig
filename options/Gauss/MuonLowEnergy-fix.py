## ############################################################################
## # File for running Gauss with Baseline Upgrade detectors and Low Energy
## # configuration for Muon Background Parameterization
## #
## # p.griffith 2019-10-18
## ############################################################################
from Configurables import Gauss
from GaudiKernel import SystemOfUnits
from Gaudi.Configuration import importOptions, appendPostConfigAction

## Load the cavern geometry
importOptions('$GAUSSOPTS/Infrastructure.py')

## Need to give "FTFP_BERT_HP_NoCherenkov" option separately - keep it
## separate to allow using it with other Physics List if/when needed
## Physics Setup
# - FTFP-BERT with HP hadronic physics list
# - RICH Cherenkov OFF 
# - One fixed interaction
# - Production cuts applied
# - Tracking cuts applied
# - Muon filter physics thresholds overridden
##

## Muon Low energy - it should include the options from Gauss
importOptions('$GAUSSOPTS/MuonLowEnergy.py')
def muonLowEnergyXMLSim():
    from Configurables import SimulationSvc
    SimulationSvc().SimulationDbLocation = "$GAUSSROOT/xml/MuonLowEnergy.xml"
appendPostConfigAction(muonLowEnergyXMLSim)

## Should set always one fixed interaction 
importOptions('$APPCONFIGOPTS/Gauss/OneFixedInteraction.py')
