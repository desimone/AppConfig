from Configurables import Generation

def redecay_eventcuttoolpatch():
    sig_gen = Generation('GenerationSignal')
    org_gen = Generation('Generation')

    # Copy the FullGenEventCutTool if it exists
    if hasattr(org_gen, 'FullGenEventCutTool'):
            org_ctl_name = org_gen.FullGenEventCutTool.split('/')[-1]
            sig_gen.FullGenEventCutTool = org_gen.FullGenEventCutTool
            # Check if the cuttool is configured
            if hasattr(org_gen, org_ctl_name):
                org_ctl = getattr(org_gen, org_ctl_name)
                sig_gen.addTool(org_ctl, org_ctl_name)

from Gaudi.Configuration import appendPostConfigAction
appendPostConfigAction(redecay_eventcuttoolpatch)
