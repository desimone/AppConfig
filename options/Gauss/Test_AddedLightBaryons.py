# option file to test effects of new light baryons added into DECAY.DEC
# for details see LHCBGAUSS-1090

from Configurables import LHCbApp, CondDB, UpdateManagerSvc
CondDB().LocalTags["DDDB"] = ["particles-20170520"]

from Configurables import ToolSvc, Gauss
def postConf():
    ToolSvc().EvtGenDecay.DecayFile = "$DECFILESROOT/dkfiles/DECAY_WithMissingBaryons.DEC"

from Gaudi.Configuration import *
appendPostConfigAction( postConf )

