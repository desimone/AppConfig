#
# File to deactivate simulation of everything past RICH2
#

from Configurables import Gauss

# Remove 'spd', 'Prs', 'Ecal', 'Hcal', 'Muon' from the list of detectors
Gauss().DetectorGeo = {"Detectors": ['PuVeto', 'Velo', 'TT', 'IT', 'OT',
                                     'Rich1', 'Rich2', 'Magnet']}
Gauss().DetectorSim = {"Detectors": ['PuVeto', 'Velo', 'TT', 'IT', 'OT',
                                     'Rich1', 'Rich2', 'Magnet']}
Gauss().DetectorMoni = {"Detectors": ['PuVeto', 'Velo', 'TT', 'IT', 'OT',
                                      'Rich1', 'Rich2', 'Magnet']}

# Reduce the particle tracking volume in the simulation
from Configurables import TrCutsRunAction
TrCutsRunAction("TrCuts").MaxZ = 12000.  # From 25000
