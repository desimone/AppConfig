#-- For Signal Particle Guns setup to use the beam spot vertex and defined
#-- in Gauss configurable
from Configurables import ParticleGun
ParticleGun().VertexSmearingTool = "BeamSpotSmearVertex"
