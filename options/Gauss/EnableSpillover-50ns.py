#-- Enable spill-over 
#-- Set the bunch spacing to 50 ns, hence spill-over for the following slots
from Configurables import Gauss
Gauss().SpilloverPaths = [ 'PrevPrev',
                           'NextNext' ]

from Configurables import GenInit
from GaudiKernel import SystemOfUnits
GenInit("GaussGen").BunchSpacing = 50 * SystemOfUnits.ns

