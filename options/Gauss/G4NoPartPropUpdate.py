# Options to tell Geant4 to keep its own particle properties and not those from
# LHCb
# Note that the spill-over MUST be set before appliying these options

from Configurables import Gauss, GenerationToSimulation

spillOverSlots = Gauss().defineCrossingList()
for slot in spillOverSlots:
    GenerationToSimulation( "GenToSim" + slot,
                            UpdateG4ParticleProperties = False )
