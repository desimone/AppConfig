# These options correspond to write out the full information, including the
# HepMC event record

from Configurables import Gauss

Gauss().OutputType = 'XSIM'
