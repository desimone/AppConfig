from Gauss.Configuration import *

#Gauss().Ready = "No"

Gauss().BeamMomentum = 4000*SystemOfUnits.GeV
Gauss().B2Momentum = -1580*SystemOfUnits.GeV
Gauss().B1Particle = 'p'
Gauss().B2Particle = 'Pb'

#--Not used since OneFixedInteraction.py is called
Gauss().Luminosity        = 1.086*(10**30)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().TotalCrossSection = 3768*SystemOfUnits.millibarn

#--Set the average position of the IP
Gauss().InteractionPosition = [  0.680*SystemOfUnits.mm ,##
                                 0.210*SystemOfUnits.mm ,##
                                 1.000*SystemOfUnits.mm ]##


#--Set the bunch RMS, this will be used for calculating the sigmaZ of the
#  Interaction Region. SigmaX and SigmaY are calculated from the beta* and
#  emittance
Gauss().BunchRMS = 63.64*SystemOfUnits.mm # 45*sqrt(2) 

#--the half effective crossing angle (in LHCb coordinate system), horizontal
#  and vertical. And tilts of the beam line
Gauss().BeamHCrossingAngle =  -0.236*SystemOfUnits.mrad
Gauss().BeamVCrossingAngle =  0.100*SystemOfUnits.mrad
Gauss().BeamLineAngles     = [ 0.0, 0.0 ]

#--beta* and emittance (beta* is nomimally 3m and e_norm 2.5um,
#                       adjusted to match sigmaX and sigmaY)
Gauss().BeamEmittance     = 0.003298*SystemOfUnits.mm
Gauss().BeamBetaStar      = 2.0*SystemOfUnits.m

from Gaudi.Configuration import importOptions
importOptions('$APPCONFIGOPTS/Gauss/OneFixedInteraction.py')
