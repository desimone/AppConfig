# File for setting hypothetical beam conditions
# Beam 7 TeV, beta* = 3m , emittance(normalized) ~ 2 micron
#
# Requires Gauss v40r0 or higher.
#
# Syntax is: 
#  gaudirun.py $APPCONFIGOPTS/Gauss/Beam7000GeV-md100-nu5.7-25ns.py
#              $DECFILESROOT/options/30000000.opts (i.e. event type)
#              $LBGENROOT/options/GEN.py (i.e. production engine)
#              MC11a-Tags.py (i.e. database tags to be used)
#              gaudi_extra_options_NN_II.py (ie. job specific: random seed,
#                               output file names, see Gauss-Job.py as example)
#
from Configurables import Gauss
from Configurables import EventClockSvc, FakeEventTime
from GaudiKernel import SystemOfUnits

#--Set the L/nbb, total cross section and revolution frequency and configure
#--the pileup tool
# We assume mu = 0.699 * nu
# Expected mu = 4.
Gauss().Luminosity        = 0.62900*(10**30)/(SystemOfUnits.cm2*SystemOfUnits.s) # nu = 5.72225

# CrossingRate = 11.245*SystemOfUnits.kilohertz used internally
Gauss().TotalCrossSection = 102.3*SystemOfUnits.millibarn

#--Set the luminous region for colliding beams and beam gas and configure
#--the corresponding vertex smearing tools, the choice of the tools is done
#--by the event type
#
# Only z is used from Gauss v40r0, the others are calcuted from beta* and emittance.
# Also in reality is the bunch RMS, not the interaction size.
Gauss().InteractionSize = [
    0.030*SystemOfUnits.mm ,
    0.030*SystemOfUnits.mm ,
    57.00*SystemOfUnits.mm
    ]

# Set interaction position
Gauss().InteractionPosition = [
    0.0*SystemOfUnits.mm ,
    0.0*SystemOfUnits.mm ,
    20.0*SystemOfUnits.mm
    ]

#--Set the energy of the beam,
#--the half effective crossing angle (in LHCb coordinate system),
#--beta* and emittance (e_norm ~ 2 microns)
Gauss().BeamMomentum      = 7.0*SystemOfUnits.TeV
# Beam crossing angle component from up-state magnet is positive.
Gauss().BeamCrossingAngle = 0.135*SystemOfUnits.mrad
Gauss().BeamEmittance     = 0.0022*SystemOfUnits.mm
Gauss().BeamBetaStar      = 3.0*SystemOfUnits.m
# Assume 0 here until we have real-world information
Gauss().BeamLineAngles    = [
    0.0 ,
    0.0
    ]

# Introduce a vertical crossing angle
# Until the configurable is formed to set this parameter we have to do this post-configuration:
def setVCrossingAngle():
    from Configurables import GenInit
    genInit = GenInit( "GaussGen" )
    genInit.VerticalCrossingAngle   = 0.100*SystemOfUnits.mrad

from Gaudi.Configuration import *
appendPostConfigAction( setVCrossingAngle )

#--Set the bunch spacing to 25 ns, hence spill-over for the following slots
Gauss().SpilloverPaths = [
    'PrevPrev' ,
    'Prev' ,
    'Next' ,
    'NextNext'
    ]


#
#--Starting time, all events will have the same
#--Can be used for beam conditions: YEBM (year,energy,bunch-spacing,field)
ec = EventClockSvc()
ec.addTool(FakeEventTime(), name="EventTimeDecoder")
ec.EventTimeDecoder.StartTime = 2701.0*SystemOfUnits.ms
ec.EventTimeDecoder.TimeStep  = 0.0

