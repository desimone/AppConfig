# postConfig-IT-OT-Position.py
# Paul Szczypka 20130226
# This file is only applicable to Minimal Upgrade Detector jobs running with Gauss v42r4
#
# Specifically, it stops a large volume of error messages being printed due to wrong
# position settings of the IT or OT, and then corrects the settings
#
from Gauss.Configuration import *
from Gaudi.Configuration import *

def fixMoniSeq():
    from Configurables import MCHitMonitor

    crossingList = [ '' ]
    spillOverList = Gauss().getProp("SpilloverPaths")
    while '' in spillOverList :
        spillOverList.remove('')
        crossingList += spillOverList

    for slot in crossingList:


        detMoniSeq = GaudiSequencer( "DetectorsMonitor" + slot )

        # Wrong settings for IT
        myZStations = [
            8015.0*SystemOfUnits.mm,
            8697.0*SystemOfUnits.mm,
            9363.0*SystemOfUnits.mm
            ]

        myZStationXMax = 150.*SystemOfUnits.cm
        myZStationYMax = 150.*SystemOfUnits.cm

        mcITHitMonToRemove = MCHitMonitor(
            "ITHitMonitor" + slot ,
            mcPathString = "MC/IT/Hits",
            zStations = myZStations,
            xMax = myZStationXMax,
            yMax = myZStationYMax
            )
        if mcITHitMonToRemove in detMoniSeq.Members:
            detMoniSeq.Members.remove(mcITHitMonToRemove)

        # Wrong Settings for OT
        myZStations = [
            7672.0*SystemOfUnits.mm,
            8354.0*SystemOfUnits.mm,
            9039.0*SystemOfUnits.mm
            ]

        mcOTHitMonToRemove = MCHitMonitor(
            "OTHitMonitor" + slot ,
            mcPathString = "MC/OT/Hits",
            zStations = myZStations,
            xMax = myZStationXMax,
            yMax = myZStationYMax
            )
        if mcOTHitMonToRemove in detMoniSeq.Members:
            detMoniSeq.Members.remove(mcOTHitMonToRemove)



###############################################

        myZStations = [
            7780.0*SystemOfUnits.mm,
            8460.0*SystemOfUnits.mm,
            9115.0*SystemOfUnits.mm
            ]
        myZStationXMax = 150.*SystemOfUnits.cm
        myZStationYMax = 150.*SystemOfUnits.cm


        detMoniSeq.Members += [
            MCHitMonitor(
            "ITHitMonitor" + slot ,
            mcPathString = "MC/IT/Hits",
            zStations = myZStations,
            xMax = myZStationXMax,
            yMax = myZStationYMax
            )
            ]

        myZStations = [
            7938.0*SystemOfUnits.mm,
            8625.0*SystemOfUnits.mm,
            9315.0*SystemOfUnits.mm
            ]
        myZStationXMax = 100.*SystemOfUnits.cm
        myZStationYMax = 100.*SystemOfUnits.cm

        detMoniSeq.Members += [
            MCHitMonitor(
            "OTHitMonitor" + slot ,
            mcPathString = "MC/OT/Hits",
            zStations = myZStations,
            xMax = myZStationXMax,
            yMax = myZStationYMax
            )
            ]


appendPostConfigAction(fixMoniSeq)

