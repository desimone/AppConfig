##############################################################################
# File for running Gauss on 2016 detector configuration
##############################################################################

from Configurables import Gauss
Gauss().DataType  = "2016"
