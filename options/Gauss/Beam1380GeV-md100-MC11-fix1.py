# File for setting Beam conditions for 2011 simulation
#
# Beam 1.38 TeV
#
# Syntax is: 
#  gaudirun.py $APPCONFIGOPTS/Gauss/Beam1380GeV-md100-MC11-fix1.py
#              $APPCONFIGOPTS/Conditions/XXXXXXX.py
#              $DECFILESROOT/options/30000000.opts (ie. event type)
#              $LBGENROOT/options/GEN.py (i.e. production engine)
#              gaudi_extra_options_NN_II.py (ie. job specific: random seed,
#                               output file names, see Gauss-Job.py as example)
#
from Configurables import Gauss
from Configurables import EventClockSvc, FakeEventTime
from GaudiKernel import SystemOfUnits

#--Set the L/nbb, total cross section and revolution frequency and configure
#--the pileup tool
Gauss().Luminosity        = 0.143*(10**30)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().TotalCrossSection = 78.4*SystemOfUnits.millibarn

#--Set the luminous region for colliding beams and beam gas and configure
#--the corresponding vertex smearing tools, the choice of the tools is done
#--by the event type
Gauss().InteractionSize = [ 0.112*SystemOfUnits.mm, 0.112*SystemOfUnits.mm,
                            7.09*SystemOfUnits.cm ]
# Only z is used from Gauss v40r0, the others are calcuted from beta* and
# emittance. Also in reality is the bunch RMS, not the interaction size.

Gauss().InteractionPosition = [ 0.0*SystemOfUnits.mm ,
                                0.0*SystemOfUnits.mm ,
                                0.0*SystemOfUnits.mm ]

#--Set the energy of the beam,
#--the half effective crossing angle (in LHCb coordinate system),
#--beta* and emittance (e_norm ~ 3.7 microns)
Gauss().BeamMomentum      = 1.38*SystemOfUnits.TeV
Gauss().BeamCrossingAngle = -0.69*SystemOfUnits.mrad
Gauss().BeamEmittance     = 0.0037*SystemOfUnits.mm
Gauss().BeamBetaStar      = 10.0*SystemOfUnits.m


#--Starting time, to identify beam conditions, all events will have the same
#--Can be used for beam conditions: YEBM (year,energy,bunch-spacing,field)
ec = EventClockSvc()
ec.addTool(FakeEventTime(), name="EventTimeDecoder")
ec.EventTimeDecoder.StartTime = 1101.0*SystemOfUnits.ms
ec.EventTimeDecoder.TimeStep  = 0.0

#-- Fix one interaction per bunch crossing!
from Configurables import Generation
Generation("Generation").PileUpTool = "FixedNInteractions"
