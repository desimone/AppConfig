from Gauss.Configuration import *
from Configurables       import MinimumBias, Inclusive, PythiaProduction

myGen = Generation("Generation")

myGen.addTool(MinimumBias())
myGen.MinimumBias.addTool(PythiaProduction())
myGen.MinimumBias.PythiaProduction.Commands = ["pydat1 parj 11 0.4",
                                               "pydat1 parj 12 0.4",
                                               "pydat1 parj 13 0.769",
                                               "pydat1 parj 14 0.09",
                                               "pydat1 parj 15 0.018",
                                               "pydat1 parj 16 0.0815",
                                               "pydat1 parj 17 0.0815"]

myGen.addTool(Inclusive())
myGen.Inclusive.addTool(PythiaProduction())
myGen.Inclusive.PythiaProduction.Commands = ["pydat1 parj 11 0.4",
                                             "pydat1 parj 12 0.4",
                                             "pydat1 parj 13 0.769",
                                             "pydat1 parj 14 0.09",
                                             "pydat1 parj 15 0.018",
                                             "pydat1 parj 16 0.0815",
                                             "pydat1 parj 17 0.0815"]
myGen.Inclusive.InclusivePIDList = [521, -521, 511, -511, 531, -531, 541, -541, 5122, -5122, 5222, -5222, 5212, -5212, 5112, -5112, 5312, -5312, 5322, -5322, 5332, -5332, 5132, -5132, 5232, -5232]
