# File for setting Beam conditions for 2011 simulation
# Does not work with Gauss < v40
#
# Beam 3.5 TeV
#
# Syntax is: 
#  gaudirun.py $APPCONFIGOPTS/Gauss/Beam3500GeV-mu100-nu3,5-50ns.py
#              $APPCONFIGOPTS/Conditions/XXXXXXX.py
#              $DECFILESROOT/options/30000000.opts (ie. event type)
#              $LBGENROOT/options/GEN.py (i.e. production engine)
#              gaudi_extra_options_NN_II.py (ie. job specific: random seed,
#                               output file names, see Gauss-Job.py as example)
#
from Gauss.Configuration import * 

#--Set the L/nbb, total cross section and revolution frequency and configure
#--the pileup tool
Gauss().Luminosity        = 0.222*(10**30)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().CrossingRate      = 11.245*SystemOfUnits.kilohertz
Gauss().TotalCrossSection = 91.1*SystemOfUnits.millibarn

#--Set the luminous region for colliding beams and beam gas and configure
#--the corresponding vertex smearing tools, the choice of the tools is done
#--by the event type
Gauss().InteractionSize = [ 0.050*SystemOfUnits.mm, 0.050*SystemOfUnits.mm,
                            50.00*SystemOfUnits.mm ] # Only z is used from Gauss v40r0, the others are calcuted from beta* and emittance

Gauss().InteractionPosition = [ 0.0*SystemOfUnits.mm ,
                                0.0*SystemOfUnits.mm ,
                                10.0*SystemOfUnits.mm ]

#--Set the energy of the beam,
#--the half effective crossing angle (in LHCb coordinate system),
#--beta* and emittance
Gauss().BeamMomentum      = 3.5*SystemOfUnits.TeV
Gauss().BeamCrossingAngle = 0.020*SystemOfUnits.mrad
Gauss().BeamEmittance     = 0.67*(10**(-9))*SystemOfUnits.rad*SystemOfUnits.m
Gauss().BeamBetaStar      = 3.0*SystemOfUnits.m
Gauss().BeamLineAngles    = [-0.085*SystemOfUnits.mrad, 0.035*SystemOfUnits.mrad]

#
#--Starting time, to identify beam conditions, all events will have the same
ec = EventClockSvc()
ec.addTool(FakeEventTime(), name="EventTimeDecoder")
ec.EventTimeDecoder.StartTime = 1311.0*SystemOfUnits.ms
ec.EventTimeDecoder.TimeStep  = 0.0

