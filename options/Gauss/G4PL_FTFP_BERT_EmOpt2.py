##
##  File containing options to activate the FTFP_BERT Hadronic
##  Physics List with EM Opt2 in Geant4 
##

from Configurables import Gauss

Gauss().PhysicsList = {"Em":'Opt2', "Hadron":'FTFP_BERT', "GeneralPhys":True, "LHCbPhys":True}
