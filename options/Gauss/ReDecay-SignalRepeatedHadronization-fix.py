# -- ReDecay in Gauss v49r7 only supports event types using SignalPlain.
# -- This file applies the fix by using SignalPlain for the redecay part of the
# -- event while copying all the necessary settings from the original
# -- SampleGenerationTool
# --
# -- This fix is planned to be included in the next Gauss release

from Gaudi.Configuration import appendPostConfigAction


def fix_redecay():
    from Configurables import RedecayProduction
    from Configurables import GaussRedecayFakePileUp
    from Configurables import Generation
    from Configurables import SignalPlain

    sig_gen = Generation('GenerationSignal')
    org_gen = Generation('Generation')

    sig_gen.EventType = org_gen.EventType
    sig_gen.SampleGenerationTool = "SignalPlain"
    sig_gen.addTool(SignalPlain)

    sig_gen.PileUpTool = "GaussRedecayFakePileUp"
    sig_gen.addTool(GaussRedecayFakePileUp)
    sig_gen.VertexSmearingTool = ""

    # Signal SampleGenerationTool
    sig_sgt = sig_gen.SignalPlain
    sig_sgt.ProductionTool = "RedecayProduction"
    sig_sgt.addTool(RedecayProduction)
    sig_sgt.RevertWhenBackward = False

    # Original SampleGenerationTool to get the PIDList and CutTool
    org_sgt_name = org_gen.SampleGenerationTool.split('/')[-1]
    org_sgt = getattr(org_gen, org_sgt_name)
    sig_sgt.SignalPIDList = org_sgt.SignalPIDList

    # Copy the CutTool if it exists
    if hasattr(org_sgt, 'CutTool'):
        org_ctl_name = org_sgt.CutTool.split('/')[-1]
        sig_sgt.CutTool = org_sgt.CutTool
        # Check if the cuttool is configured, might not be in case of
        # simple ones like DaughtersInLHCb
        if hasattr(org_sgt, org_ctl_name):
            org_ctl = getattr(org_sgt, org_ctl_name)
            sig_sgt.addTool(org_ctl, org_ctl_name)


appendPostConfigAction(fix_redecay)
