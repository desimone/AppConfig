from Configurables import Generation
from Configurables import FlatZSmearVertex
#reduce the Z production zone to [-450,250]mm
Generation("Generation").VertexSmearingTool = "FlatZSmearVertex"
Generation("Generation").addTool( FlatZSmearVertex )
Generation("Generation").FlatZSmearVertex.ZMin = -450.
Generation("Generation").FlatZSmearVertex.ZMax =  250.
