from __future__ import print_function
# ---------------------------------------------------------------------------


class GaussMPpatch_1409(object):

    # Workarounds for LHCBGAUSS-1409
    # This patch is necessary for all Gauss versions tested so far (>= v51r0)
    @staticmethod
    def postConfigActions():
        print('--- Workarounds for LHCBGAUSS-1409 (start) ---')
        from Configurables import ApplicationMgr
        print('Disable StalledEventMonitoring')
        ApplicationMgr().StalledEventMonitoring = False
        print('--- Workarounds for LHCBGAUSS-1409 (end)   ---')

    # Apply the workarounds for LHCBGAUSS-1409
    @staticmethod
    def apply():
        print('--- Workarounds for LHCBGAUSS-1409 (start) ---')
        print('Append post-config action to disable StalledEventMonitoring')
        from Gaudi.Configuration import appendPostConfigAction
        appendPostConfigAction(GaussMPpatch_1409.postConfigActions)
        print('--- Workarounds for LHCBGAUSS-1409 (end)   ---')

# ---------------------------------------------------------------------------


class GaussMPpatch_1779_1780_1785(object):

    # Workarounds for LHCBGAUSS-1779
    # This is a patch over the source code of Gaudi v30r3 (used by Gauss v53r1)
    # It is necessary for Gauss v52r0 and above
    # It is not necessary, but harmless, for Gauss v51r0 and below (no '/FileRecords/GenFSR' expected)
    @staticmethod
    def SendFileRecords(self):
        # send the FileRecords data as part of finalisation
        # Take Care of FileRecords!
        # There are two main things to consider here
        # 1) The DataObjects in the FileRecords Transient Store
        # 2) The fact that they are Keyed Containers, containing other objects
        #
        # The Lead Worker, nodeID=0, sends everything in the FSR store, as
        #   a completeness guarantee,
        #
        # send in form ( nodeID, path, object)
        print('Entering FileRecordsAgent.SendFileRecords from GaussMPpatch (workarounds for LHCBGAUSS-1779)')
        from GaudiPython import SUCCESS
        import pickle
        self.log.info('Sending FileRecords...')
        lst = self.fsr.getHistoNames()
        # Check Validity
        if not lst:
            self.log.info('No FileRecords Data to send to Writer.')
            self.q.put('END_FSR')
            return SUCCESS
        # no need to send the root node
        if '/FileRecords' in lst:
            lst.remove('/FileRecords')
        for l in lst:
            o = self.fsr.retrieveObject(l)
            if hasattr(o, 'configureDirectAccess'):
                o.configureDirectAccess()
            # lead worker sends everything, as completeness guarantee
            if self._gmpc.nodeID == 0:
                self.objectsOut.append((0, l, pickle.dumps(o)))
            else:
                # only add the Event Counter
                # and non-Empty Keyed Containers (ignore empty ones)
                if l == '/FileRecords/EventCountFSR':
                    tup = (self._gmpc.nodeID, l, pickle.dumps(o))
                    self.objectsOut.append(tup)
                elif l == '/FileRecords/GenFSR':
                    print('--- Workarounds for LHCBGAUSS-1779 (start) ---')
                    print('WARNING! Ignoring %s' % l)
                    self.log.warning('Ignoring %s' % l)
                    print('--- Workarounds for LHCBGAUSS-1779 (end)   ---')
                else:
                    # It's a Keyed Container
                    assert 'KeyedContainer' in o.__class__.__name__
                    nObjects = o.numberOfObjects()
                    if nObjects:
                        self.log.debug('Keyed Container %s with %i objects'
                                       % (l, nObjects))
                        tup = (self._gmpc.nodeID, l, pickle.dumps(o))
                        self.objectsOut.append(tup)
        self.log.debug('Done with FSR store, just to send to Writer.')
        if self.objectsOut:
            self.log.debug('%i FSR objects to Writer' % (len(self.objectsOut)))
            for ob in self.objectsOut:
                self.log.debug('\t%s' % (ob[0]))
            self.q.put(self.objectsOut)
        else:
            self.log.info('Valid FSR Store, but no data to send to Writer')
        self.log.info('SendFSR complete')
        self.q.put('END_FSR')
        return SUCCESS

    # -----------------------------------------------------------------------

    # Workarounds for LHCBGAUSS-1785 (sub-issue of LHCBGAUSS-1780)
    # This patch is deployed as a part of the workarounds for LHCBGAUSS-1780
    # This patch is necessary for Gauss v53r1 and above
    # It should be avoided for Gauss v52r2 and below
    # It is pointless for Gauss v53r0, where there is no workaround for LHCBGAUSS-1780
    class EvtCounterWrapper(object):
        def __init__(self, cntr):
            print('Creating a wrapper over the EvtCounter tool (workaround for LHCBGAUSS-1785)')
            from Configurables import LbAppInit, GenInit
            if hasattr(LbAppInit, 'EvtCounter'):
                # This is v52r2 or earlier
                print('Property EvtCounter exists in LbAppInit: old Gauss version <= v52r2, no need to apply the workaround')
                print('NB: "Nr. in job" represents the event number across all workers')
            else:
                # This is v53r0 or later
                print('Property EvtCounter does not exist in LbAppInit: new Gauss version >= v53r0, apply the workaround')
                print('NB: "Nr. in job" represents the event number in each worker, not across all workers')
            self.cntr = cntr

        def setEventCounter(self, evt):
            print('New event: number in job across all workers = %s' % evt)
            self.cntr.setEventCounter(evt)

    # -----------------------------------------------------------------------

    # Workarounds for LHCBGAUSS-1780
    # This is a patch over the source code of Gaudi v30r3 (used by Gauss v53r1)
    # It is necessary for Gauss v53r1 and above
    # It is not necessary, but harmless, for Gauss v52r2 and below (no need to look for GaussGen.EvtCounter, as ToolSvc.EvtCounter exists)
    # *** No workaround for this issue is available for Gauss v53r0 ***
    @staticmethod
    def Initialize(self):
        print('Entering GMPComponent.Initialize from GaussMPpatch (workarounds for LHCBGAUSS-1780/1785)')
        from GaudiPython import InterfaceCast, gbl
        import time
        start = time.time()
        self.processConfiguration()
        self.SetupGaudiPython()
        self.initEvent.set()
        self.StartGaudiPython()
        if self.app == 'Gauss':
            print('fetch tool ToolSvc.EvtCounter')
            tool = self.a.tool('ToolSvc.EvtCounter')
            self.cntr = InterfaceCast(gbl.IEventCounter)(tool.getInterface())
            if self.cntr is None:
                print('--- Workarounds for LHCBGAUSS-1780 (start) ---')
                print('IEventCounter is still:', self.cntr)
                print('fetch tool GaussGen.EvtCounter')
                tool = self.a.tool('GaussGen.EvtCounter')
                self.cntr = InterfaceCast(
                    gbl.IEventCounter)(tool.getInterface())
                print('--- Workarounds for LHCBGAUSS-1780 (end)   ---')
            print('IEventCounter is now:', self.cntr)
        else:
            self.cntr = None
            print('IEventCounter is None')
        print('--- Workarounds for LHCBGAUSS-1785 (start) ---')
        self.cntr = GaussMPpatch_1779_1780_1785.EvtCounterWrapper(self.cntr)
        print('--- Workarounds for LHCBGAUSS-1785 (end)   ---')
        self.iTime = time.time() - start

    # -----------------------------------------------------------------------

    # Dummy replacement for module GaudiMP.GMPBase
    # This was developed against the source code of Gaudi v30r3 (used by Gauss v53r1)
    class GaudiMP(object):
        class GMPBase(object):
            @staticmethod
            def Coord(nWorkers, config, log):
                print('--- Workarounds for LHCBGAUSS-1779/1780/1785 (start) ---')
                print('Entering Coord from GaussMPpatch (workarounds for LHCBGAUSS-1779/1780/1785)')
                # Delete GaudiMP and GaudiMP.GMPBase dummy modules
                print('Delete GaudiMP and GaudiMP.GMPBase dummy modules')
                import sys
                del sys.modules['GaudiMP']
                del sys.modules['GaudiMP.GMPBase']
                # Override pTools.FileRecordsAgent.SendFileRecords
                print('Override pTools.FileRecordsAgent.SendFileRecords (workarounds for LHCBGAUSS-1779)')
                from GaudiMP.pTools import FileRecordsAgent
                FileRecordsAgent.SendFileRecords = GaussMPpatch_1779_1780_1785.SendFileRecords
                # Override GMPBase.GMPComponent.Initialize
                print('Override GMPBase.GMPComponent.Initialize (workarounds for LHCBGAUSS-1780/1785)')
                from GaudiMP import GMPBase
                GMPBase.GMPComponent.Initialize = GaussMPpatch_1779_1780_1785.Initialize
                # Return a Coord from the original GaudiMP.GMPBase
                print('Return a Coord from the original GaudiMP.GMPBase')
                import GaudiMP.GMPBase as gpp
                Parall = gpp.Coord(nWorkers, config, log)
                print('--- Workarounds for LHCBGAUSS-1779/1780/1785 (end)   ---')
                return Parall

    # -----------------------------------------------------------------------

    # Apply the workarounds for LHCBGAUSS-1779/1780/1785
    @staticmethod
    def apply():
        # Override module GaudiMP.GMPBase
        print('--- Workarounds for LHCBGAUSS-1779/1780/1785 (start) ---')
        print('Override module GaudiMP.GMPBase')
        import sys
        # ensure 'import GaudiMP.GMPBase as gpp' defines gpp as the patched GaudiMP.GMPBase
        sys.modules['GaudiMP'] = GaussMPpatch_1779_1780_1785.GaudiMP
        sys.modules['GaudiMP.GMPBase'] = 0  # avoid 'No module named GMPBase'
        print('--- Workarounds for LHCBGAUSS-1779/1780/1785 (end)   ---')

# ---------------------------------------------------------------------------


class GaussMPpatch_1786(object):

    # Workarounds for LHCBGAUSS-1786
    # This patch is necessary for Gauss v53r1 and above
    # It should be avoided for Gauss v52r2 and below
    # It is pointless for Gauss v53r0, where there is no workaround for LHCBGAUSS-1780
    @staticmethod
    def postConfigActions():
        print('--- Workarounds for LHCBGAUSS-1786 (start) ---')
        from Configurables import LbAppInit, GenInit
        if hasattr(LbAppInit, 'EvtCounter'):
            # This is v52r2 or earlier
            print('Property EvtCounter exists in LbAppInit: old Gauss version <= v52r2, no need to apply the workaround')
        else:
            # This is v53r0 or later
            print('Property EvtCounter does not exist in LbAppInit: new Gauss version >= v53r0, apply the workaround')
            print('GenInit(GaussGen).FirstEventNumber was', GenInit(
                'GaussGen').FirstEventNumber)
            GenInit('GaussGen').FirstEventNumber = GenInit(
                'GaussGen').FirstEventNumber - 1
            print('GenInit(GaussGen).FirstEventNumber is now', GenInit(
                'GaussGen').FirstEventNumber)
        print('--- Workarounds for LHCBGAUSS-1786 (end)   ---')

    # Apply the workarounds for LHCBGAUSS-1786
    @staticmethod
    def apply():
        print('--- Workarounds for LHCBGAUSS-1786 (start) ---')
        print('Append post-config action to decrease FirstEventNumber by 1')
        from Gaudi.Configuration import appendPostConfigAction
        appendPostConfigAction(GaussMPpatch_1786.postConfigActions)
        print('--- Workarounds for LHCBGAUSS-1786 (end)   ---')

# ---------------------------------------------------------------------------


class GaussMPpatch_preV53R1(object):

    # Backward compatibility patch for Gauss v53r0 or earlier
    @staticmethod
    def enablePreV53R1():
        from Configurables import Gauss
        if not hasattr(Gauss, 'isGaussMP'):  # backport the patch to Gauss v53r0 and earlier
            # This is v53r0 or earlier
            print('Method Gauss.isGaussMP does not exist: this is Gauss <= v53r0')
            print('Implement method Gauss.isGaussMP as in Gauss v53r1')
            Gauss.isGaussMP = GaussMPpatch_preV53R1.isGaussMP
            # Check if this is v52r2 or earlier
            from Configurables import LbAppInit, GenInit
            if hasattr(LbAppInit, 'EvtCounter'):
                # This is v52r2 or earlier
                print('Property EvtCounter exists in LbAppInit: this is Gauss <= v52r2')
                GaussMPpatch_preV53R1.isGaussV53R0 = False
            else:
                # This is v53r0 or later
                print('Property EvtCounter does not exist in LbAppInit: this is Gauss >= v53r0')
                GaussMPpatch_preV53R1.isGaussV53R0 = True
        else:
            # This is v53r1 or later
            print('Method Gauss.isGaussMP exists: this is Gauss >= v53r1')
            GaussMPpatch_preV53R1.isGaussV53R0 = False

    # Workaround for missing isGaussMP method in Gauss v53r0 and earlier
    # This is the isGaussMP() implementation from Gauss v53r1
    @staticmethod
    def isGaussMP(self):
        import argparse
        parser = argparse.ArgumentParser(description='Hello')
        parser.add_argument('--ncpus', action='store', type=int, default=0)
        args = parser.parse_known_args()
        return args[0].ncpus != 0

    # Is this Gauss v53r0?
    isGaussV53R0 = False


# ---------------------------------------------------------------------------

print('=== GaussMPpatch (start) ===')
GaussMPpatch_preV53R1.enablePreV53R1()
from Configurables import Gauss
if Gauss().isGaussMP():
    print('This is GaussMP: apply the patch')
    if GaussMPpatch_preV53R1.isGaussV53R0:
        raise Exception(
            'GaussMPpatch: no workaround for LHCBGAUSS-1780 is possible in Gauss v53r0')
    GaussMPpatch_1409.apply()
    GaussMPpatch_1779_1780_1785.apply()
    GaussMPpatch_1786.apply()
else:
    print('This is not GaussMP: no need to apply the patch')
print('=== GaussMPpatch (end)   ===')
