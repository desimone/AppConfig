from Configurables import Generation
from Configurables import MinimumBias
from Configurables import Inclusive
from Configurables import Pythia8Production



commandsTuning_2 = [
    'Init:showAllSettings                = on',

    'SpaceShower:rapidityOrder           = off',          # General
    'SpaceShower:alphaSvalue             = 0.130',
    'MultipartonInteractions:alphaSvalue = 0.130',

    'StringFlav:mesonSvector             = 7.474387e-01', # Flavour selection
    'StringFlav:probQQtoQ                = 1.615701e-01',
    'StringFlav:probStoUD                = 3.501613e-01',
    
    'MultipartonInteractions:bProfile    = 2',            # Multiparton interactions
    'MultipartonInteractions:pT0Ref      = 2.693335e+00'
    ]


gaussGen = Generation("Generation")
gaussGen.addTool(MinimumBias, name = "MinimumBias")
gaussGen.MinimumBias.ProductionTool = "Pythia8Production"
gaussGen.MinimumBias.addTool(Pythia8Production, name = "Pythia8Production")

gaussGen.MinimumBias.Pythia8Production.Commands += commandsTuning_2


gaussGen.addTool(Inclusive, name = "Inclusive")
gaussGen.Inclusive.ProductionTool = "Pythia8Production"
gaussGen.Inclusive.addTool(Pythia8Production, name = "Pythia8Production")

gaussGen.Inclusive.Pythia8Production.Commands += commandsTuning_2

