# Disable flat spillover. Option available starting from Boole v20r2
# Typical use case is for low luminosity simulation, e.g. for 2009 conditions
from Configurables import Boole
Boole().IgnoreFlatSpillover = True
