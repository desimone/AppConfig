##############################################################################
# File for running Boole with Upgrade configuration
##############################################################################

from Boole.Configuration import *


from Configurables import LHCbApp, CondDB

CondDB().Upgrade = True

if "Calo_NoSPDPRS" in CondDB().AllLocalTagsByDataType:
    CondDB().AllLocalTagsByDataType.remove("Calo_NoSPDPRS")
 
from Configurables import Boole
for det in ["Spd", "Prs"]:
    if det not in Boole().DetectorDigi:
        Boole().DetectorDigi.append(det)

for det in ["Spd", "Prs"]:
    if det not in Boole().DetectorLink:
        Boole().DetectorLink.append(det)

for det in ["Spd", "Prs"]:
    if det not in Boole().DetectorMoni:
        Boole().DetectorMoni.append(det)

Boole().DataType = "Upgrade"

