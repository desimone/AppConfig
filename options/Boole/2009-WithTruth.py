##############################################################################
# Options for running Boole including emulation of 2009 L0 trigger
#
# Syntax is:
# gaudirun.py Boole/2009-WithTruth.py Conditions/<someTag>.py <someDataFiles>.py
##############################################################################

from Configurables import Boole

# just instantiate the configurable
theApp = Boole()

##############################################################################
# Options to get the right L0 settings
##############################################################################
from Configurables import L0DUAlg, L0CaloAlg

L0Calo = L0CaloAlg('L0Calo')
L0Calo.AddECALToHCAL= False
L0DU = L0DUAlg('L0DU')
L0DU.TCK = '0x1309'
L0DU.BankVersion = 1

from Gaudi.Configuration import importOptions
importOptions("$APPCONFIGOPTS/L0/L0Muon-FOI-2009.py")
