# $Id: Upgrade-WithTruth.py,v 1.2 2009-10-02 10:32:13 tskwarni Exp $
##############################################################################
# File for running Boole with Upgrade VeloPix configuration, with full Digi output
#
# Syntax is:
# gaudirun.py Boole/Upgrade-VeloPix-WithTruth.py Conditions/<someTag>.py <someDataFiles>.py
##############################################################################

from Configurables import Boole

# just instantiate the configurable
theApp = Boole()
# set special data type for upgrade simulations
theApp.DataType  = "Upgrade"

# use VeloPix instead of Velo, switch L0 off
theApp.DetectorDigi["VELO"]=['VeloPix']
theApp.DetectorDigi["L0"]=[]
theApp.DetectorLink["VELO"]=['VeloPix']
theApp.DetectorLink["L0"]=[]
# v21r8 now need TR
#theApp.DetectorLink["TR"]=[]
theApp.DetectorMoni["VELO"]=['VeloPix']
theApp.DetectorMoni["L0"]=[]


# enable spillover
theApp.UseSpillover = True
#   
from Configurables import DigiConf
DigiConf().SpilloverPaths = ["Prev", "PrevPrev", "Next"]
##############################################################################

from Gaudi.Configuration import *
OutputStream( "DigiWriter").OptItemList += ["/Event/pSim/VeloPix#1","/Event/pSim/VeloPix/Hits#1"]
