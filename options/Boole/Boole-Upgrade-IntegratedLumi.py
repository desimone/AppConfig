from Configurables import VPDepositCreator
from Configurables import MCFTDepositCreator, MCFTG4AttenuationInterpolationTool, FTSiPMTool

# integrated luminosity in fb^-1
lumi=5.

# VP
VPDepositCreator().Irradiated = True
VPDepositCreator().DataTaken = lumi
#VPDepositCreator().RiseTime = lumi #declared as GaudiProperty but unused 

# FT
MCFTDepositCreator().AttenuationToolName = "MCFTG4AttenuationInterpolationTool" 
MCFTDepositCreator().addTool(MCFTG4AttenuationInterpolationTool("MCFTG4AttenuationInterpolationTool"))
MCFTDepositCreator().MCFTG4AttenuationInterpolationTool.IrradiationLevel = lumi
# Aging of fibres in months since production (between July 2016 and December 2017)
# Fibre age to be placed in SIMCOND if inhomogeneities will be relevant
# Set to mid-point of 2022 data-taking period (Jul 2022) and mid-point of production period (Feb 2017)
MCFTDepositCreator().MCFTG4AttenuationInterpolationTool.FibreAge = 65 

# See definitions in FTSiPMTool.h of noise calculation, assumes scaled to 
# 6 x 10^11 neq/cm^2 at 50 fb^-1 (giving 14HMz thermal noise at 40deg)
noise = (6./50.) * lumi

MCFTDepositCreator().addTool(FTSiPMTool("FTSiPMTool"))
MCFTDepositCreator().FTSiPMTool.IrradiationLevel = noise 
