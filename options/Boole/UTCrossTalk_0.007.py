from Configurables import MCSTDepositCreator
MCUTDepositCreator = MCSTDepositCreator('MCUTDepositCreator', DetType='UT')

MCUTDepositCreator.XTalkParamsLeftOdd   = [0.07, 0]
MCUTDepositCreator.XTalkParamsLeftEven  = [0.07, 0]
MCUTDepositCreator.XTalkParamsRightOdd  = [0.07, 0]
MCUTDepositCreator.XTalkParamsRightEven = [0.07, 0]

