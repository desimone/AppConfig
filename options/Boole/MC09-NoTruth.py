##############################################################################
# File for running Boole with MC09 configuration, with Minimal Digi output
#
# Syntax is:
# gaudirun.py Boole/MC09-NoTruth.py Conditions/<someTag>.py <someDataFiles>.py
##############################################################################

from Configurables import Boole

# Switch off MC truth output except for primary vertices information
Boole().DigiType = 'Minimal'

##############################################################################
