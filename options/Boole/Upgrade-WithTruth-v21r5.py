# $Id: Upgrade-WithTruth.py,v 1.2 2009-10-02 10:32:13 tskwarni Exp $
##############################################################################
# File for running Boole with Upgrade configuration, with full Digi output
#
# Syntax is:
# gaudirun.py Boole/Upgrade-WithTruth.py Conditions/<someTag>.py <someDataFiles>.py
##############################################################################

from Configurables import Boole

# just instantiate the configurable
theApp = Boole()
# set special data type for upgrade simulations
theApp.DataType  = "Upgrade"
# upgrade uses old-style Velo simulation 
theApp.VeloTell1Processing = False
# enable spillover
theApp.UseSpillover = True
#   
# from Configurables import DigiConf
# DigiConf().SpilloverPaths = ["Prev", "PrevPrev", "Next"]
##############################################################################
