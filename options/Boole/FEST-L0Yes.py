##############################################################################
# File for Boole production of MDF files for Full Experiment System Test
# selecting only events passing L0 or random trigger
#
# Syntax is:
# gaudirun.py Boole/FEST-L0Yes.py Conditions/<someTag>.py <someDataFiles>.py
##############################################################################

from Configurables import Boole

Boole().Outputs    = ["MDF"]

# Write out only events passing L0, or with simulated random trigger
Boole().FilterSequence = ["L0","ODIN"]

# Do not compress output file, for faster reading by FEST injector
from Configurables import LHCb__RawDataCnvSvc
LHCb__RawDataCnvSvc().Compress = 0
