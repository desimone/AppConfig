## Options to switch off all geometry (and related simulation)
## save that of calorimeters area.
##
## Author: P.Szczypka
## Date:   2012-12-20
##
from Configurables import Boole
Boole().SiG4EnergyDeposit = True

