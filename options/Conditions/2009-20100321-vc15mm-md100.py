##############################################################################
# File for defining conditions with Velo closed at 15 mm and "Down" magnetic
# field with geometry as described on Jan 2010, in the same conditions as for
# 2009 data.
##############################################################################

from Configurables import LHCbApp

LHCbApp().DDDBtag   = "head-20100119"
LHCbApp().CondDBtag = "MC-20100321-vc15mm-md100"

##############################################################################
