##############################################################################
# File for defining conditions with Velo over-closed 
# Velo 2 half position defined as measured in  Ted Oct data (open by ~290
# micron in x with the resolver position at 0,0) and simulating
# overclosing by 200 micron by ovveriding the resolver position
##############################################################################
from Configurables import LHCbApp, CondDB, UpdateManagerSvc
CondDB().LocalTags["SIMCOND"] = ["velo-tedoct-2halfTxTyRxRy"]
#over-close by 200 micron
UpdateManagerSvc().ConditionsOverride +=  ["Conditions/Online/Velo/MotionSystem := double ResolPosRC =0.100 ; double ResolPosLA = -0.100 ;"]


##############################################################################
