##############################################################################
# File for defining conditions with Velo closed
# Velo at the nominal position both for sensors and 2 halves
##############################################################################
from Configurables import LHCbApp, CondDB, UpdateManagerSvc
CondDB().LocalTags["SIMCOND"] = ["velo-no_metrology"]


##############################################################################
