##############################################################################
# File for defining Upgrade conditions with Velo closed and "Down" magnetic field
##############################################################################

from Configurables import LHCbApp

# this is needed since sets different DDDB file
LHCbApp().DataType  = "Upgrade"
# VeloPix on top of Minimal Upgrade Layout on top of head-20091112
LHCbApp().DDDBtag   = "velopix-mul-20091116"
# same as for head-20091112
LHCbApp().CondDBtag = "sim-20091112-vc-md100"

##############################################################################
