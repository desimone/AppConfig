##############################################################################
# File for defining Upgrade conditions with Velo closed and "Down" magnetic field
##############################################################################

from Configurables import LHCbApp

# this is needed since sets different DDDB file
LHCbApp().DataType  = "Upgrade"
# Minimal Upgrade Layout with aerogel out of the acceptance
# notice that Boole and Brunel will have to use mul-20090917 tag to process
#    these data
LHCbApp().DDDBtag   = "mul-gauss-20090917"
# same as for MC09
LHCbApp().CondDBtag = "sim-20090402-vc-md100"

##############################################################################
