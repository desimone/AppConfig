##############################################################################
# File for defining Upgrade conditions with Velo closed and "Down" magnetic field
##############################################################################

from Configurables import LHCbApp

# this is needed since sets different DDDB file
LHCbApp().DataType  = "Upgrade"
# Minimal Upgrade Layout
#   this tag can be used to run Boole or Brunel also for data
#   generated with "mul-noaerogel-20100617" 
#   here aerogel is in
LHCbApp().DDDBtag   = "mul-20100617"
# same as for underlying head-20100504 DDDB tag 
LHCbApp().CondDBtag = "sim-20100510-vc-md100"

##############################################################################
