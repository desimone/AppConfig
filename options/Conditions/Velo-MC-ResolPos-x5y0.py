# Options to pick up velo close at 5 mm as in 2.68 TeV in 2011, assuming
# vertex at x,y = 0,0
from Configurables import CondDB

CondDB().LocalTags["SIMCOND"] = ["velo-20110307"]


