### Options to have magnet off in MC, hence applied to SIMCOND
### All applications will need it

from Configurables import CondDB
CondDB().LocalTags['SIMCOND'] = ['magnet-off']
