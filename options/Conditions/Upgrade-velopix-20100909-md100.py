##############################################################################
# File for defining Upgrade conditions with Velo closed and "Down" magnetic field
##############################################################################

from Configurables import LHCbApp

# this is needed since sets different DDDB file
LHCbApp().DataType  = "Upgrade"
# L-shape VeloPix on top of Minimal Upgrade Layout "mul-20100617"
LHCbApp().DDDBtag   = "velopix-mul-20100909"
# same as for mul-20100617
LHCbApp().CondDBtag = "sim-20100510-vc-md100"

##############################################################################
