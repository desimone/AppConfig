##############################################################################
# File for running with FT_MonoLayer Upgrade configuration
##############################################################################
from Configurables import CondDB
CondDB().Upgrade = True

# Add the FT_MonoLayer tag
if "FT_StereoAngle2" not in CondDB().AllLocalTagsByDataType:
    CondDB().AllLocalTagsByDataType += ["FT_StereoAngle2"]

# Remove the FT tag
if "FT" in CondDB().AllLocalTagsByDataType:
    CondDB().AllLocalTagsByDataType.remove("FT")




