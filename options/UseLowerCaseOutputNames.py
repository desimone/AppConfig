# Author: Marco Clemencic
#
# Change the names of all the output files replacing the extension
# (anything after the first '.') with its lowercase version:
#
#  MyFile.AnExtension.dst --> MyFile.anextension.dst
#
from Gaudi.Configuration import allConfigurables, appendPostConfigAction
import re

def useLowerCaseOutputNames():
    # Regular expression to find the extension of the output file name
    patt = re.compile(r"DATAFILE='[^'.]*\.([^']*)'")
    # loop over all the configurables
    for c in allConfigurables.values():
        if hasattr(c, 'Output'): # must have an Output property set
            m = patt.search(c.Output)
            if m: # if it matches the regular expression
                # replace the extension with its lowercase version
                a, b = m.span(1)
                c.Output = c.Output[:a] + m.group(1).lower() + c.Output[b:]

appendPostConfigAction(useLowerCaseOutputNames)
