#!/usr/bin/env python
#################################################################################

from __future__ import print_function
import urllib, sys, os, re, array, string, commands

#############################################################################

def check_stdout(logFile):

  global logString
  global stringFile

# --- read error list from external input (ASCII file)
  
  readErrorDict(stringFile,dict_G4_errors)

# --- open the Project log file (check if exist + if not empty) as a String
  ok = getLogString(logFile)
  if not ok:
      print('WARNING: Problems in reading ', logFile)
      return

# --- create a nested dictionary with:
# --- main dictionary: keys = errors_string - nested dictionary
# --- nested disctionary: keys=id counters - values=error dumps

# --- sort dict keys in reverse order - help to test errors double counting
  reversedKeys = sorted(dict_G4_errors.keys(), reverse=True)

  for errString in reversedKeys:

    dict_count_dump_errString = dict()

    ctest = logString.count(errString)
    test = logString.find(errString)

    for i in range(0,ctest):
      start = test
      test = logString.find(errString,start)
# --- check if the current error analyzed was already taken into account with previous strings
      foundAlready = False
      for err in reversedKeys:
        if err == errString:
          break
        checke = logString[test:test+100].find(err)
        if checke != -1:
          foundAlready = True
          test = test + len(err)
          break

      if foundAlready: continue 


      if test != -1:        
         eventnr=''
         runnr=''
# --- search for event / run number 
         eventnr_point =  logString.rfind('INFO Evt',test-5000,test)
         if eventnr_point != -1:
           eventnr = 'Evt ' + logString[eventnr_point:test].split('INFO Evt')[1].strip().split(',')[0]
           runnr = logString[eventnr_point:].split('INFO Evt')[1].strip().split(',')[1]

# --- search for error dump           
# --- in G4 error cases
         if errString.find('G4') != -1 :
           check = logString[test:test+250].find('***')
           if check != -1:
             error_base = logString[test:test+250].split('***')[0]
             dict_count_dump_errString[i] = eventnr +"  "+runnr+"  -->"+error_base 
             lenght_dump = len(error_base)
             test = test + lenght_dump
         else:
# --- in all other error cases
             error_base = logString[test:test+250].split('\n')[0]
             dict_count_dump_errString[i] = eventnr +"  "+runnr+"  -->"+error_base
             lenght_dump = len(error_base)
             test = test + lenght_dump


    dict_G4_errors_count[errString] = dict_count_dump_errString

    
  HTMLtable(dict_count_dump_errString,dict_G4_errors_count,"errors.html")

#####################################################
def HTMLtable(dict_count_dump_errString,dict_G4_errors_count,name):
  f = open(name,'w')
  f.write("<HTML>\n")

  f.write("<table border=1 bordercolor=#000000 width=100% bgcolor=#BCCDFE>")
  f.write("<tr>")
  f.write("<td>ERROR TYPE</td>")
  f.write("<td>COUNTER</td>")
  f.write("<td>DUMP OF ERROR MESSAGES</td>")
  f.write("</tr>")

  orderedKeys = sorted(dict_G4_errors_count.keys())
  for errString in orderedKeys:
    if dict_G4_errors_count[errString] !={}:
      f.write("<tr>")
      f.write("<td>"+errString+"</td>")
      f.write("<td>"+str(len(dict_G4_errors_count[errString].keys()))+"</td>")
      f.write("<td>")
      f.write("<lu>")
      for y in dict_G4_errors_count[errString].keys():
        f.write("<li>")
        f.write(" "+dict_G4_errors_count[errString][y]+" ")
      f.write("</lu>")
      f.write("</td>")
      f.write("</tr>")

  f.write("</table>")

  return 

#############################################################################
def getLogString(logFile):

  global logString,ok

  ok = False
  print("Attempting to open %s" % logFile)
  if not os.path.exists(logFile):
    print('%s is not available' % logFile)
    return
  if os.stat(logFile)[6] == 0:
    print('%s is empty' % logFile)
    return
  fd = open(logFile,'r')

  logString = fd.read()

  ok = True

  return ok

#################################################################
def getLines(stringFile):

# --- get stringFile content (line by line) ---

  print(">>> processed STRINGFILE -> ", stringFile)
  f = open(stringFile,'r')
  lines = f.readlines()
  f.close()
  return lines


#################################################################
def readErrorDict(stringFile,dictName):
  
  fileLines = getLines(stringFile)

  for line in fileLines:
           errorstring = line.split(',')[0]
           description = line.split(',')[1]
           dictName[errorstring] = description
  
  return

################################################################
def pickStringFile(project,version):

   sourceDir = commands.getoutput('echo $APPCONFIGROOT') + '/errstrings' 
   
   fileString = project+'_'+version+'_errs.txt'
   stringFile = os.path.join(sourceDir, os.path.basename(fileString))

# if string file for required version does not exist -> take the latest
   if not os.path.exists(stringFile):
     print('string file %s does not exist, trying to take the most recent one for this project...' % stringFile)
     filelist = [os.path.join(sourceDir, f) for f in os.listdir(sourceDir) if f.find(project)!=-1]
     if 0 != len(filelist):
       filelist.sort()
       stringFile = filelist[len(filelist)-1]
     else:
       print('WARNING: no string files (any versions) for this project ')
       return None           
     
   return stringFile

################################################################################
# Usage:
#
# python LogErr.py project_log_file.log  projectName version
#
# --------------------
# --- main program ---
# --------------------

global logString
global ok
global stringFile

# --- read arguments ---
logFile= sys.argv[1]
projName = sys.argv[2]
verName = sys.argv[3]

# --- select the proper error string file for the input version 
stringFile = pickStringFile(projName,verName)

# --- initialization
logString = ''
ok = ''
dict_G4_errors = dict()
dict_G4_errors_count = dict()

# --- analyse stdout file - update counters 
if stringFile != None:
  if os.stat(stringFile)[6] != 0:
    check_stdout(logFile)
  else:
    print('WARNING: string file %s is empty' % stringFile)




